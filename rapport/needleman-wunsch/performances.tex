\subsection{Needleman-Wunsch}

\subsubsection{Table des scores}

Nous avons implémenté 3 versions de la fonction table des scores. La première avec les outils de python, une seconde en utilisant Numpy pour optimiser l'utilisation de la RAM, et une dernière utilisant les avantages des 2 précedantes.

Toutes ces fonctions reposent sur le même principe, il y a un boucle qui itère sur les lignes et une autre qui itère sur les colonnes. Si on note \emph n le nombre de lignes et \emph m le nombre de colonne, on a une complexité dans le temps en $O(n.m)$. La complexité également en $O(n.m)$ car c'est un tableau de \emph n par \emph m.

Nous avons donc tracé un graphique en modifiant la taille de la première séquence d'ADN et en laissant la seconde inchangée. On s'attend donc à avoir une courbe linéaire.

\begin{figure}[H]
    \centering
    \includesvg[width=1\columnwidth]{rapport/performances/comparaison_table_des_scores.svg}
    \caption{Performances en fonction de la longueur d'une séquence}
    \label{fig:perfs_table_des_scores_fonctions}
\end{figure}

On observe en effet que les différentes courbes sont linéaires. Nous avons utilisé une échelle logarithmique pour mettre en évidence le rapport entre les différentes courbes.

On peut donc remarquer que la seconde version (en utilisant uniquement Numpy) est beaucoup plus lente (environ 10 fois plus lente) que les deux autres fonctions, ce qui explique pourquoi nous avions besoin d'éviter au maximum l'indexage de Numpy à cause de sa lenteur.

On remarque en revanche que la dernière version, utilisant au maximum les listes de python pour les calculs donne des résultats similaires à la première version, tout en consommant environ 6 fois moins de RAM (\autoref{section:opti_ram}), ce qui est un très gros gain.

De plus, on s'attend à ce que les performances soient symétriques sur les deux séquences (ie: on peut appliquer l'algorithme sur \emph{(seqA, seqB)} ou \emph{(seqB, seqA)} indifférement en termes de performances). On a donc voulu le vérifier:

\begin{figure}[H]
    \centering
    \includesvg[width=1\columnwidth]{rapport/performances/comparaison_table_des_scores_sym.svg}
    \caption{Performances en fonction de la longueur d'une séquence}
    \label{fig:perfs_table_des_scores_sym}
\end{figure}

On observe en effet que faire varier la première ou la seconde séquence a le même effet sur les performances.


\subsubsection{Alignements optimaux}

Pour le calcul des alignements optimaux, nous avons essayé de faire plusieurs optimisations avant d'opter pour la version avec un générateur (\autoref{section:opti_ram}).

La première idée était de sauvegarder les alignements retournés par la fonction récursive à chaque case. L'intérêt serait de n'executer l'algorithme pour chaque case qu'une seule fois (si on repasse sur une case déjà visitée, on connaît déjà le résultat). On peut s'attendre à avoir une évolution beaucoup plus lente du temps d'execution de l'algorithme en fonction du nombre d'alignements optimaux trouvés par rapport à la fonction initiale.

Une seconde idée part du fait que seulement 100\% du CPU (c'est à dire l'utilisation de seulement un coeur du processeur). On a donc voulu faire du \emph{multiprocessing} pour utiliser tous les coeurs du processeur.

La fonction suit le schéma suivant:

\begin{enumerate}
  \item Si on est dans la case en haut à gauche, on communique au processus principal l'alignement trouvé par le chemin parcouru et on termine le processus.
  \item On cherche les cases adjacentes à traiter
  \item Pour chaques nouvelles cases (sauf une qui sera traitée par le processus actuel) on créé un nouveau processus
  \item Dans chaque processus, on lance cette fonction sur une des cases trouvées à l'étape 1.
\end{enumerate}

Avant d'utiliser cette fonction, on créé un nouveau processus. Le processus parent "écoutera" les autres processus pour lister les alignements et le processus enfant lancera la fonction sur la dernière case du tableau.

Dans le pire des cas, la fonction aura une complexité linéaire en fonction du nombre d'alignements, et une complexité en $O(n+m)$ par rapport aux longueurs des séquences (respectivement \emph n et \emph m).

\begin{figure}[H]
    \centering
    \includesvg[width=1\columnwidth]{rapport/performances/alignements_optimaux_selon_nombre_dalignements.svg}
    \caption{Performances en fonction du nombre d'alignements optimaux}
    \label{fig:perfs_alignements_nombre_dalig}
\end{figure}

Pour tracer ces courbes, on a tenté d'avoir des séquences de longueur à peu près constantes et de ne modifier que le nombre d'alignements.

On observe bien des courbes à peu près linéaires (tout de même légèrement convexe en particulier pour le cas du mulitprocessing. Cela est dû à la longueur des séquences qui augmente légèrement en même temps que le nombre d'alignements).

Pour ce qui est des performances des différents algorithmes, on voit que comme on s'y attendait, la version avec les sauvegardes des alignements pour les cases déjà parcourues augmente plus lentement que la version initiale. Il faut tout de même noter que cette version consomme beaucoup plus de RAM et ne peut donc pas être utilisée dans le cas de séquences de 30.000 par 30.000 (ce qui est le cas que l'on cherche à traiter).

En revanche, on remarque également que la version faisant du multiprocessing est bien plus lente alors qu'elle utilise beaucoup plus de ressources. En fait, chaque \textit{fork} (création d'un nouveau processus) demande beaucoup de ressources à l'ordinateur, et dans cet algorithme il y en a très fréquement ce qui consomme plus de puissance que ce que l'on gagne de cette manière.

Nous avons également cherché à tracer l'évolution du temps d'execution en fonction de la longueur des séquences (nous avons fait évoluer la longueur des deux séquences simultanément)

\begin{figure}[H]
    \centering
    \includesvg[width=1\columnwidth]{rapport/performances/alignements_optimaux_selon_longueurs.svg}
    \caption{Performances en fonction de la longueur des séquences}
    \label{fig:perfs_alignements_longueur}
\end{figure}

Nous avons bien une évolution en $n+m$ comme on l'avait annoncé.

On remarque qu'encore une fois la version utilisant du multiprocessing est très lente par rapport aux autres. On peut également noter que la version avec sauvegarde est moins performante pour de grandes séquences (ce qui peut s'expliquer par le nombre d'alignements à sauvegarder qui croît également).

Pour finir, nous avons également testé le temps que met le générateur (solution adoptée pour le produit final) pour passer d'un alignement à un autre en fonction de la longueur des séquences. Nous avons essayer de simuler "le pire des cas" pour avoir une majoration de ce temps. Encore une fois, on a une complexité en $O(n+m)$.

\begin{figure}[H]
    \centering
    \includesvg[width=1\columnwidth]{rapport/performances/alignements_optimaux_selon_longueurs_generateur.svg}
    \caption{Performances du génerateur en fonction de la longueur des séquences}
    \label{fig:perfs_alignements_longueur_generateur}
\end{figure}

On obtient bien le résultat attendu.

\vspace{4mm}

Une dernière que nous n'avons pas pu implémenté est basé sur l'observation suivante: les séquences sur lesquelles on applique Needleman-Wunsch sont généralement très semblables. On pourrait donc essayer de localiser leurs différences pour n'appliquer needleman que sur ces séquences en appliquant l'algorithme suivant:

\begin{enumerate}
    \item On itère sur les deux séquences tant qu'elles sont identiques.
    \item Lorsqu'on trouve une différence, on cherche quand les séquences sont de nouveau identiques puis on enregistre les sous-séquences différentes.
    \item On applique Needleman-Wunsch sur ces sous-séquences.
    \item On compte le nombre de "trous" qu'il y a au maximum dans une des deux séquences dans les alignements trouvé (on le nommera \emph n pour la suite)
    \item On fait récupère de nouveau les sous-séquences en les commençant n bases avant et en les terminants n bases après.
    \item On applique de nouveau Needleman-Wunsch sur ces 2 nouvelles sous-séquences.
    \item Si on ne trouve aucun nouvel alignement alors on a tout les alignements optimaux correspondant à cette différence.
    \item Sinon on recommence depuis l'étape 4 jusqu'à ne plus trouver de nouveaux alignements.
    \item Pour chaques alignements trouvé, on concatène les "sous-alignements" trouvés avec ceux trouvés sur des sous-séquences différentes traitée précedement (en complétant avec les sous-séquences identiques entre ces sous-séquences différentes)
\end{enumerate}

Cette manière de procéder pourrait être très rapide si on trouve une manière simple de chercher la fin des différences et de gérer le cas où en augmentant la tailles des sous-séquences traitées on déborde sur des différences déjà traité.

Si on appliquait cet algorithme sur les séquences "\emph{AACGTACGT}" et "\emph{ACGTAACGT}" on appliquerait Needleman-Wunsch sur le couple "\emph{A}" et "", on trouverait l'alignement "\emph{A}" / "\emph{-}".
En élargissant cet alignement de 1 (puisqu'il y a un trou) on aurait les alignements "\emph{AA}" / "\emph{A-}" et "\emph{AA}" / "\emph{-A}". Il y a un alignement supplémentaire, on continue donc.
On l'applique ensuite sur "\emph{AAC}" et "\emph{AC}" et on trouve autant d'alignements que précedement, on peut donc s'arreter et retenir les derniers alignements trouvés:
"\emph{AAC}" / "\emph{A-C}" et "\emph{AAC}" / "\emph{-AC}".

On continue a itérer sur la séquence jusqu'au couple "" et "\emph{A}". Sur le même principe, on trouve finalement les alignements "\emph{T-ACG}" / "\emph{TAACG}" et "\emph{TA-CG}" / "\emph{TAACG}".

On trouve donc finalement 4 alignements:

\begin{itemize}
	\item \emph{\textbf{AAC}G\textbf{T-ACG}T} / \emph{\textbf{A-C}G\textbf{TAACG}T}
	\item \emph{\textbf{AAC}G\textbf{TA-CG}T} / \emph{\textbf{A-C}G\textbf{TAACG}T}
	\item \emph{\textbf{AAC}G\textbf{T-ACG}T} / \emph{\textbf{-AC}G\textbf{TAACG}T}
	\item \emph{\textbf{AAC}G\textbf{TA-CG}T} / \emph{\textbf{-AC}G\textbf{TAACG}T}
\end{itemize}
