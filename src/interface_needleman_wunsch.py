""" fichier contenant les classes necessaires à l'affichage de needleman-wunsch """
# pylint: disable=no-name-in-module, invalid-name, unsubscriptable-object
from typing import List, Optional, Tuple, Generator
from PyQt5.QtWidgets import QWidget, QTextEdit, QLabel, QVBoxLayout, QPushButton, QHBoxLayout, QTableView, QSplitter #type: ignore
from PyQt5.QtGui import QTextOption, QColor #type: ignore
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QAbstractItemModel, QVariant, QModelIndex #type: ignore
from qdarkstyle import DarkPalette #type: ignore

from src.needleman_wunsch import alignements_optimaux

class NeedlemanWunsch(QWidget):
    """Classe pour l'affichage des résultats de Needleman-Wunsch"""

    iterate = pyqtSignal(QWidget, bool)
    renderAlignements = pyqtSignal()

    def __init__(self) -> None:
        super().__init__()

        self.seqA = self.seqB = self.seqAnom = self.seqBnom = ""
        self.gen: Optional[Generator[Tuple[Tuple[str, str], List[List[int]], List[Tuple[int, int]], int], Optional[bool], None]] = None
        self.alignement = ("", "")
        self.matrice = [[0]]
        self.pile: List[Tuple[int, int]] = []
        self.index = -1

        self.calculThread = None

        self.suivant = QPushButton("Suivant >>")
        self.suivant.clicked.connect(self.suivant_handler)

        self.precedant = QPushButton("<< Précédant")
        self.precedant.clicked.connect(self.precedant_handler)

        self.score = QLabel()
        self.score.setAlignment(Qt.AlignCenter)

        self.header = QWidget()
        headerWidget = QHBoxLayout()
        headerWidget.addWidget(self.precedant)
        headerWidget.addWidget(self.score)
        headerWidget.addWidget(self.suivant)

        self.header.setLayout(headerWidget)

        alignements = QWidget()

        alignementAbox = QWidget()
        self.alignementA = QTextEdit(readOnly = True)
        self.labelA = QLabel()
        self.labelA.setAlignment(Qt.AlignHCenter)
        layoutA = QVBoxLayout()
        layoutA.addWidget(self.labelA)
        layoutA.addWidget(self.alignementA)
        alignementAbox.setLayout(layoutA)

        alignementBbox = QWidget()
        self.alignementB = QTextEdit(readOnly = True)
        self.labelB = QLabel()
        self.labelB.setAlignment(Qt.AlignHCenter)
        layoutB = QVBoxLayout()
        layoutB.addWidget(self.labelB)
        layoutB.addWidget(self.alignementB)
        alignementBbox.setLayout(layoutB)

        self.alignementA.setWordWrapMode(QTextOption.WrapAnywhere)
        self.alignementB.setWordWrapMode(QTextOption.WrapAnywhere)

        self.alignementA.verticalScrollBar().valueChanged.connect(
                self.alignementB.verticalScrollBar().setValue
                )

        self.alignementB.verticalScrollBar().valueChanged.connect(
                self.alignementA.verticalScrollBar().setValue
                )

        self.alignementA.setFontFamily("monospace")
        self.alignementB.setFontFamily("monospace")

        self.alignementA.setFontPointSize(20)
        self.alignementB.setFontPointSize(20)

        self.alignementA.cursorPositionChanged.connect(self.cursor_a_to_b)
        self.alignementB.cursorPositionChanged.connect(self.cursor_b_to_a)

        alignementsLayout = QHBoxLayout()
        alignementsLayout.addWidget(alignementAbox)
        alignementsLayout.addWidget(alignementBbox)

        alignements.setLayout(alignementsLayout)

        alignementsLayout = QHBoxLayout()

        self.table = QTableView()

        itemModel = NeedlemanWunschItemModel()
        self.table.setModel(itemModel)

        self.layout = QVBoxLayout()

        splitter = QSplitter(orientation=Qt.Vertical)
        splitter.addWidget(alignements)
        splitter.addWidget(self.table)

        self.layout.addWidget(self.header)
        self.layout.addWidget(splitter)

        self.setLayout(self.layout)

        self.renderAlignements.connect(self.render_widgets)
        self.iterate.connect(CalculNeedleman.launch)

    def cursor_a_to_b(self) -> None:
        """
        Lorsque le curseur de la zone de texte de la séquence A est modifié, on met à jour celui de la séquence B.

        :rtype: None
        """
        self.alignementB.setTextCursor(self.alignementA.textCursor())

    def cursor_b_to_a(self) -> None:
        """
        Lorsque le curseur de la zone de texte de la séquence B est modifié, on met à jour celui de la séquence A.

        :rtype: None
        """
        self.alignementA.setTextCursor(self.alignementB.textCursor())

    def set_sequences(self, seqA: str, seqAnom: str, seqB: str, seqBnom: str) -> None:
        """
        Change les séquences du widget et met à jour les composants de ce widget.

        :param seqA: Séquence A
        :type seqA: str
        :param seqAnom: Nom de la séquence A
        :type seqAnom: str
        :param seqB: Séquence B
        :type seqB: str
        :param seqBnom: Nom de la séquence B
        :type seqBnom: str
        :rtype: None
        """
        self.matrice = [[0]]
        self.alignement = ("", "")
        self.pile = []
        self.index = 0
        self.seqA = seqA
        self.seqAnom = seqAnom
        self.seqB = seqB
        self.seqBnom = seqBnom
        self.render_widgets()
        self.gen = alignements_optimaux(seqA, seqB, loop=True)
        self.iterate.emit(self, False)

    def render_widgets(self) -> None:
        """
        Met à jour tout les composants du widget

        :rtype: None
        """
        self.labelA.setText(self.seqAnom)
        self.labelB.setText(self.seqBnom)

        self.alignementA.setText(self.alignement[0])
        self.alignementB.setText(self.alignement[1])

        self.score.setText(f"Score : {self.matrice[-1][-1]}     -     Alignement n°{self.index}")

        itemModel = NeedlemanWunschItemModel(self.matrice, self.pile, self.seqA, self.seqB)
        self.table.setModel(itemModel)

    def iterate_handler(self, sens: bool) -> None:
        """
        Gère les itérations sur le génerateur de Needleman-Wunsch

        :param sens: Si cette variable vaut `True`, le génerateur va aller vers l'alignement précédant
        :type sens: bool
        :rtype: None
        """
        if self.gen:
            try:
                if sens:
                    self.alignement, self.matrice, self.pile, self.index = self.gen.send(sens)
                else:
                    self.alignement, self.matrice, self.pile, self.index = next(self.gen)
                self.renderAlignements.emit()
            except RuntimeError:
                pass


    def precedant_handler(self) -> None:
        """
        Gère l'appui sur le bouton "précédant"

        :rtype: None
        """
        self.iterate.emit(self, True)

    def suivant_handler(self) -> None:
        """
        Gère l'appui sur le bouton "suivant"

        :rtype: None
        """
        self.iterate.emit(self, False)

class NeedlemanWunschItemModel(QAbstractItemModel):
    """Classe pour la gestion de l'affichage de la matrice de Needleman-Wunsch"""
    def __init__(self, table: Optional[List[List[int]]] = None, pile:Optional[List[Tuple[int, int]]]=None, seqA:str="", seqB:str="") -> None:
        super().__init__()
        self.table = table if table is not None else [[0]]
        self.pile = pile if pile is not None else []
        self.seqA = seqA
        self.seqB = seqB

    def rowCount(self, _: QModelIndex) -> int:
        """
        Retourne le nombre de lignes du tableau

        :param parent: Parent (mais ici, c'est inutile)
        :type parent: QModelIndex
        :rtype: int
        """
        return len(self.table)

    def columnCount(self, _: QModelIndex) -> int:
        """
        Retourne le nombre de colonnes du tableau

        :param parent: Parent (mais ici, c'est inutile)
        :type parent: QModelIndex
        :rtype: int
        """
        return len(self.table[0])

    def index(self, lig: int, col: int, parent: QModelIndex) -> QModelIndex:
        """
        Retourne le QModelIndex qui correspond aux paramètres donnés

        :param lig: Ligne
        :type lig: int
        :param col: Colonne
        :type col: int
        :param parent: Parent
        :type parent: QModelIndex
        :rtype: QModelIndex
        """
        return self.createIndex(lig, col, parent)

    def headerData(self, index: int, direction: Qt.Orientation, role: int) -> QVariant:
        """
        Retourne le QVariant correspondant aux paramètres donnés pour les headers
        Ici, on veut afficher dans les headers les caractères des séquences A et B

        :param index: Index de la case voulue
        :type index: int
        :param direction: Horizontal ou vertical
        :type direction: Qt.Orientation
        :param role: Role du QVariant voulu (couleur, contenu, etc.)
        :type role: int
        :rtype: QVariant
        """
        if role != Qt.DisplayRole:
            return QVariant()
        if index == 0:
            return ""
        if direction == Qt.Horizontal:
            return self.seqA[index-1]
        if direction == Qt.Vertical:
            return self.seqB[index-1]
        return QVariant()

    def data(self, index: QModelIndex, role: int) -> QVariant:
        """
        Retourne le QVariant correspondant aux paramètres donnés pour le contenu du tableau

        :param index: Index de la case voulue
        :type index: QModelIndex
        :param role: Role du QVariant voulu
        :type role: int
        :rtype: QVariant
        """
        if role == Qt.TextAlignmentRole:
            return Qt.AlignHCenter | Qt.AlignVCenter
        if not index.isValid():
            return QVariant()
        if role == Qt.DisplayRole:
            return int(self.table[index.row()][index.column()])
        if role == Qt.BackgroundRole:
            color: QColor = QColor(DarkPalette.COLOR_BACKGROUND_DARK)
            if (index.row(), index.column()) in self.pile:
                color = QColor(0, 255, 0)
            return color
        if role == Qt.ForegroundRole:
            color = QColor(255, 255, 255)
            if (index.row(), index.column()) in self.pile:
                color = QColor(0, 0, 0)
            return color
        return QVariant()

class CalculNeedleman(QThread):
    """
    Classe permettant de threader le calcul de Needleman-Wunsch (pour pas freeze l'interface)
    """

    def __init__(self, widget: QWidget, sens:bool) -> None:
        super().__init__()
        self.widget = widget
        self.sens = sens

    def run(self) -> None:
        """
        Fonction que le thread va executer

        :rtype: None
        """
        self.widget.iterate_handler(self.sens)

    @classmethod
    def launch(cls, widget: QWidget, sens: bool) -> None:
        """
        Créé et lance le thread

        :param widget: Widget lanceur
        :type widget: QWidget
        :param sens: Sens dans lequel on doit lancer
        :type sens: bool
        :rtype: None
        """
        if widget.calculThread is None or not widget.calculThread.isRunning():
            thread = cls(widget, sens)
            widget.calculThread = thread
            thread.start()
