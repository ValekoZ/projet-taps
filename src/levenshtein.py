"""
Fichier fonctions de calcul de la distance de Levenshtein
"""
from multiprocessing import Queue #type: ignore

def distance_pour_test(motA: str, motB: str) -> int:
    """
    Fonction de calcul de la distance de Levenshtein en calculant tous les chemins opératoires possibles

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :return: La distance de Levenshtein entre A et B
    :rtype: int
    """
    ligneActuelle = list(range(len(motB) + 1))
    # les déplacements entre lignes correspondent à des insertions, entre colonnes à des effacement

    for indiceA in range(1, len(motA) + 1):
        nouvelleLigne = [indiceA] + [0] * len(motB)
        # on met [indiceA] en première colonne pour avoir la première valeur corespondant au coût d'une suite
        # d'insertion

        for indiceB in range(1, len(motB) + 1):
            coutSubstitution = int(motA[indiceA - 1] != motB[indiceB - 1])
            # Aucun cout de substitution si la lettre est la même
            nouvelleLigne[indiceB] = min(ligneActuelle[indiceB] + 1, ligneActuelle[indiceB - 1] + coutSubstitution, nouvelleLigne[indiceB - 1] + 1)
            #   min ( cout effacement / cout substitution / cout insertion )
        ligneActuelle = nouvelleLigne[:]  # On ne garde en mémoire que la ligne précédente pour le calcul
    return ligneActuelle[-1]


def distance_efficace(motA: str, motB: str) -> int:
    """
    Fonction de calcul de la distance de Levenshtein de manière efficace en choisissant un nombre restreint de
    chemins opératoires

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :return: La distance de Levenshtein entre A et B
    :rtype: int
    """
    if len(motA) > len(motB): # Pour ne pas traiter deux fois les cas symétriques
        motA, motB = motB, motA

    ligneActuelle = list(range(len(motB) + 1))
    # les déplacements entre lignes correspondent à des insertions, entre colonnes à des effacement sur le motB pour aller au motA

    indicesDebuts = [0]  # liste des indices réels par rapport à la liste initiale des indices 0 de la nouvelle liste

    for indiceA, _ in enumerate(motA):

        debut = int(not indicesDebuts[-1])  # si notre indice 0 correspond réellement au début de notre liste initial
        indiceActuel = sum(indicesDebuts)  # indice réel de la liste initiale pour l'indice 0 de la nouvelle liste
        longueur = len(ligneActuelle) - 1 + debut  # longueur de notre nouvelle liste
        nouvelleLigne = [0] * (longueur)

        # Si l'indice correspond réellement à l'indice 0, le coût vaut celui d'une suite d'insertion (soit le numéro
        # de la ligne)
        if debut:
            nouvelleLigne[0] = indiceA + 1
        else:
            nouvelleLigne[0] = min(ligneActuelle[1] + 1, ligneActuelle[0] + int(motA[indiceA] != motB[indiceActuel-1]))


        indiceMin = 0  # Premier indice minimum

        for indiceB in range(1, longueur):

            coutSubstitution = int(motA[indiceA] != motB[indiceActuel + indiceB - 1])
            # Aucun cout de substitution si la lettre est la même

            if debut:
                nouvelleLigne[indiceB] = min(ligneActuelle[indiceB] + 1, ligneActuelle[indiceB - 1] + coutSubstitution, nouvelleLigne[indiceB - 1] + 1)
            else:
                nouvelleLigne[indiceB] = min(ligneActuelle[indiceB + 1] + 1, ligneActuelle[indiceB] + coutSubstitution, nouvelleLigne[indiceB - 1] + 1)
            #   min ( cout effacement / cout substitution / cout insertion )

            if nouvelleLigne[indiceB] < nouvelleLigne[indiceMin]: # recherche en parallèle du premier minimum
                indiceMin = indiceB

        indicesDebuts += [indiceMin]  # indices des éléments minimaux repris au long de notre parcours

        # On ne garde en mémoire que les elements à droite et l'élément de gauche du premier élément minimum de ligne
        # précédente pour le calcul
        nouvelIndice = max(0, indicesDebuts[-1] - 1)
        ligneActuelle = nouvelleLigne[nouvelIndice:]
    return ligneActuelle[-1]

def distance_process(queue: Queue, mot1: str, mot2: str) -> None:
    """
    ecrit le resultat de distance_efficace dans Queue
    :param queue: canal de communication entre les processus
    :type queue: Queue
    :param mot1: 1ère séquence
    :type mot1: str
    :param mot2: 2ème séquence
    :type mot2: str
    """
    queue.put(distance_efficace(mot1, mot2))
