"""fichier relatif à l'interface"""
# pylint: disable=no-name-in-module, invalid-name
import sys
import os
from typing import Tuple, Dict, List

import qdarkstyle #type: ignore

from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QStyleFactory, QAction, qApp, QFileDialog, QListWidget # type: ignore
from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QHBoxLayout, QSizePolicy, QMessageBox, QListWidgetItem, QTabWidget # type: ignore
from PyQt5.QtWidgets import QDialogButtonBox, QRadioButton, QDialog #type: ignore
from PyQt5.QtGui import QIcon, QCloseEvent #type: ignore
from PyQt5.QtCore import Qt, QThread #type: ignore

from src.lecture import lire_fasta_entier, lire_texte
from src.needleman_wunsch import kill_table_des_scores
from src.transcription import arn_en_codons, adn_en_arn, arn_en_adn, verif, T_ADN, T_ARN, T_PROTEINE
from src.interface_visualisation import Visualisation
from src.interface_denombrement import DenombrementWidget
from src.interface_suite_uniques_repetees import SuitesUniqueRepeteesWidget
from src.interface_plus_longue_suite import PlusLongueSuite
from src.interface_levenshtein import CalculLevenshtein, LevenshteinWidget
from src.interface_needleman_wunsch import NeedlemanWunsch



class MainWindow(QMainWindow):
    """
    classe de la fenetre principale
    """
    def __init__(self) -> None:
        super().__init__()
        self.sequences : Dict[str, str]
        self.listesSequencesListWidget = [QListWidget(), QListWidget(), QListWidget()]
        self.typeSelectionne = T_ADN
        self.comp = 0
        self.listesSequencesItem : List[QListWidgetItem] = []
        self.pathDir = ".."
        self.typeOk = False
        self.typeChoisi = T_ADN

        self.calculL : QThread

        self.init_ui()

        self.show()

    def init_ui(self) -> None:
        """
        crée tout les éléments graphique nécessaire à l'interface
        """
        self.setWindowTitle('Projet TAPS 2021')
        self.setStyle(QStyleFactory.create("Fusion"))
        self.showMaximized()

        # exit button
        self.exitAct = QAction(QIcon(), 'Quitter', self)
        self.exitAct.setShortcut('Ctrl+Q')
        self.exitAct.setStatusTip("Quitter l'application")
        self.exitAct.triggered.connect(self.quit)

        # directory button
        self.selectDir = QAction(QIcon(), 'Dossier', self)
        self.selectDir.setShortcut('Ctrl+D')
        self.selectDir.setStatusTip("Selectionner un dossier")
        self.selectDir.triggered.connect(self.set_dir)

        # open button
        self.open = QAction(QIcon(), 'Ouvrir', self)
        self.open.setShortcut('Ctrl+O')
        self.open.setStatusTip("select a parameters file")
        self.open.triggered.connect(self.get_file)

        # effacer menu button
        self.selectClean = QAction(QIcon(), 'Effacer', self)
        self.selectClean.setShortcut('Ctrl+N')
        self.selectClean.setStatusTip("Efface tout les fichiers chargés")
        self.selectClean.triggered.connect(self.effacer)

        # add button
        self.buttonOpen = QPushButton('Ajouter')
        self.buttonOpen.clicked.connect(self.get_file)

        # clean button
        self.buttonClean = QPushButton("Effacer")
        self.buttonClean.clicked.connect(self.effacer)

        # transcription button
        self.buttonTranscription = QPushButton("Transcrire")
        self.buttonTranscription.clicked.connect(self.transcrire)
        self.buttonTranscription.setEnabled(False)

        # fenetre pour selectionner le type de sequences ajoutées
        self.typeLoadDialog = QDialog()
        self.typeLoadDialog.setWindowTitle("Choix du type des séquences ajoutées")
        rb1 = QRadioButton("ADN")
        rb1.clicked.connect(self.est_adn)
        rb1.setChecked(True)
        rb2 = QRadioButton("ARN")
        rb2.clicked.connect(self.est_arn)
        rb3 = QRadioButton("Protéine")
        rb3.clicked.connect(self.est_proteine)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.typeLoadDialog.accept)
        buttonBox.rejected.connect(self.typeLoadDialog.close)
        layout = QHBoxLayout()
        layout.addWidget(rb1)
        layout.addWidget(rb2)
        layout.addWidget(rb3)
        layout.addWidget(buttonBox)
        self.typeLoadDialog.setLayout(layout)
        self.typeLoadDialog.finished.connect(self.set_type_ok)

        # menu des onglets
        self.tabWidget = QTabWidget()
        self.tabWidget.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        # onglet visualisation
        self.visualisationWidget = Visualisation()
        self.tabWidget.addTab(self.visualisationWidget, "Visualiser")

        # onglet denombrement
        self.denombrementWidget = DenombrementWidget()
        self.denombrementWidget.hide()
        self.tabWidget.addTab(self.denombrementWidget, "Sous-séquences")
        self.tabWidget.setTabEnabled(1, False)

        # onglet suites uniques
        self.suitesUniquesRepeteesWidget = SuitesUniqueRepeteesWidget()
        self.suitesUniquesRepeteesWidget.hide()
        self.tabWidget.addTab(self.suitesUniquesRepeteesWidget, "Suites Uniques/Répétées")
        self.tabWidget.setTabEnabled(2, False)

        # onglet plus longue suite
        self.plusLongueSuiteWidget = PlusLongueSuite()
        self.plusLongueSuiteWidget.hide()
        self.tabWidget.addTab(self.plusLongueSuiteWidget, "Plus Longue Suite")
        self.tabWidget.setTabEnabled(3, False)

        # onglet levenshtein
        self.levenshteinWidget = LevenshteinWidget()
        self.levenshteinWidget.hide()
        self.tabWidget.addTab(self.levenshteinWidget, "Levenshtein")
        self.tabWidget.setTabEnabled(4, False)

        # onglet needleman_wunsch
        self.needlemanWunschWidget = NeedlemanWunsch()
        self.needlemanWunschWidget.hide()
        self.tabWidget.addTab(self.needlemanWunschWidget, "Needleman-Wunsch")
        self.tabWidget.setTabEnabled(5, False)

        # menu des types
        self.typeTabWidget = QTabWidget()
        self.typeTabWidget.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)

        # onglet ADN
        self.typeTabWidget.addTab(self.listesSequencesListWidget[T_ADN], "ADN")

        # onglet ARN
        self.typeTabWidget.addTab(self.listesSequencesListWidget[T_ARN], "ARN")

        # onglet Codon
        self.typeTabWidget.addTab(self.listesSequencesListWidget[T_PROTEINE], "Protéine")

        # menu
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(self.selectDir)
        fileMenu.addAction(self.open)
        fileMenu.addAction(self.selectClean)
        fileMenu.addAction(self.exitAct)

        # agencement de l'interface
        hlayout = QHBoxLayout()
        vlayout = QVBoxLayout()
        vlayout.addWidget(self.typeTabWidget)
        hbox = QHBoxLayout()
        hbox.addWidget(self.buttonOpen)
        hbox.addWidget(self.buttonClean)
        vlayout.addLayout(hbox)
        vlayout.addWidget(self.buttonTranscription)
        hlayout.addLayout(vlayout)
        self.mainLayout = QVBoxLayout()
        self.mainLayout.addWidget(self.tabWidget)
        hlayout.addLayout(self.mainLayout)

        self.setCentralWidget(QWidget())
        self.centralWidget().setLayout(hlayout)

        # connection des signaux aux listes de séquences
        for liste in self.listesSequencesListWidget:
            liste.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
            liste.itemDoubleClicked.connect(lambda item : item.setCheckState(Qt.Checked if item.checkState() == Qt.Unchecked else Qt.Unchecked))
            liste.itemChanged.connect(self.update_comp)
            liste.currentItemChanged.connect(self.update_visualisation)

        # connection du signal de la typeTabWidget
        self.typeTabWidget.currentChanged.connect(self.selectionner_type)

        # sinal de tabWidget
        self.tabWidget.currentChanged.connect(self.update_show)

    def update_show(self, index : int) -> None:
        """ update le widget que l'utilisateur voit, cache les autres """

        widgets : List[QWidget] = [self.visualisationWidget, self.denombrementWidget, self.suitesUniquesRepeteesWidget, self.plusLongueSuiteWidget, self.levenshteinWidget, self.needlemanWunschWidget]
        for widget in widgets:
            if self.tabWidget.indexOf(widget) == index:
                widget.show()
            else:
                widget.hide()

    def set_dir(self) -> None:
        """
        permet à l'utlisateur de choisir le dossier d'ouverture par défaut quand il cherche à ouvrir un fichier
        """
        pathDir = QFileDialog.getExistingDirectory(parent=self, directory='..', caption="Select Directory")
        # print("pathDir", pathDir)
        if pathDir:
            #   If Windows, change the separator
            if os.sep == '\\':
                pathDir = self.pathDir.replace('/', '\\')
            self.pathDir = pathDir

    def get_file(self) -> None:
        """
        demande à l'utilisateur de choisir le fichier à charger et si il choisi un fichier, le dossier dans lequel il se trouve devient le dossier d'ouverture par défaut
        """
        path = QFileDialog.getOpenFileName(self, directory=self.pathDir,
                                           caption="Select file",
                                           filter="File (*.fasta *.txt *.fa)")
        if path[0]:
            self.load_file(path[0])
            self.pathDir = path[0][:path[0].rfind('/')]

    def load_file(self, path : str) -> None:
        """
        charge toutes les séquences présentes dans le fichier .fasta dont le chemin est données en parametre
        :param path: chemin du fichier
        :typeFile path: String
        """
        _, typeFile = path.rsplit('.', 1)
        listeSeq = []
        if typeFile in ["fasta", "fa"]:
            listeSeq = lire_fasta_entier(path)
        elif typeFile == "txt":
            seq = lire_texte(path)
            listeSeq = [seq]
        self.typeLoadDialog.exec_()
        if self.typeOk:
            for seq in listeSeq:
                self.ajouter_item(seq, self.typeChoisi)
        if self.listesSequencesListWidget[self.typeSelectionne].count() != 0 and self.listesSequencesListWidget[self.typeSelectionne].currentItem() is None:
            self.listesSequencesListWidget[self.typeSelectionne].setCurrentRow(self.listesSequencesListWidget[self.typeSelectionne].count() - 1)

    def set_type_ok(self, result : int) -> None:
        """
        appelé quand l'utilisateur valide son choix du type de sequence a ajouter. Met le booleen correspondante au resultat retourné dans typeOk
        :param result: booleen qui correspond a vrai si l'utilisateur a terminé correctement le choix
        :type result: int
        """
        self.typeOk = bool(result)

    def est_adn(self) -> None:
        """
        appelé quand le bouton ADN est selectionné. Met la valeur de typeChoisi a T_ADN
        """
        self.typeChoisi = T_ADN

    def est_arn(self) -> None:
        """
        appelé quand le bouton ARN est selectionné. Met la valeur de typeChoisi a T_ARN
        """
        self.typeChoisi = T_ARN

    def est_proteine(self) -> None:
        """
        appelé quand le bouton Proteine est selectionné. Met la valeur de typeChoisi a T_PROTEINE
        """
        self.typeChoisi = T_PROTEINE

    def ajouter_item(self, seq : Tuple[str, str], seqType : int) -> None:
        """
        ajoute un item a ma liste passée en parametre
        :param seq: tuple comprennant le nom et la sequence à ajouter
        :type seq: Tuple[str, str]
        :param seqType: type de sequence
        :type seqType: int
        """
        # if seqType in (T_ARN, T_PROTEINE):
        #     try:
        #         seq = (seq[0], adn_en_arn(seq[1]))
        #     except ValueError:
        #         print('erreur conversion adn-arn', seq)
        #     if seqType == T_PROTEINE:
        #         try:
        #             seq = (seq[0], arn_en_codons(seq[1]))
        #         except ValueError:
        #             print('erreur conversion arn-codon', seq)
        # if verif('*'.join(seq[1]) if seqType == T_PROTEINE else seq[1], seqType):
        if verif(seq[1], seqType):
            if seq[0] not in [self.listesSequencesListWidget[seqType].item(i).text() for i in range(self.listesSequencesListWidget[seqType].count())]:
                qitem = QListWidgetItem()
                qitem.setText(seq[0])
                qitem.setCheckState(Qt.Unchecked)
                qitem.setData(Qt.UserRole, seq[1])
                self.listesSequencesListWidget[seqType].addItem(qitem)
        elif seqType == T_ADN:
            QMessageBox.warning(self, 'Erreur Fichier', "le fichier ne contient pas que des séquences d'ADN")
        elif seqType == T_ARN:
            QMessageBox.warning(self, 'Erreur Fichier',
                                "le fichier ne contient pas que des séquences d'ARN ou qui n'ont pas pu etre convertie en ARN")
        elif seqType == T_PROTEINE:
            QMessageBox.warning(self, 'Erreur Fichier',
                                "le fichier ne contient pas que des séquences de Codon ou qui n'ont pu etre convertie en Codon")

    def update_comp(self, item : QListWidgetItem) -> None:
        """
        vérifie que l'utilisateur n'a pas sélectionné plus de 2 séquences , sinon lui envoie un message l'avertissant
        :param item: élement de la liste cliqué
        :type item: QListWidgetItem
        """
        self.comp = 0
        for i in range(self.listesSequencesListWidget[self.typeSelectionne].count()):
            if self.listesSequencesListWidget[self.typeSelectionne].item(i).checkState() == Qt.Checked:
                self.comp += 1

        if self.comp == 0:
            self.clear()
        elif self.comp == 1:
            if item.checkState() == Qt.Checked:
                self.listesSequencesItem.append(item)
            else:
                self.listesSequencesItem.remove(item)
                self.kill_calcul()
            self.update_widgets()
        elif self.comp == 2:
            self.listesSequencesItem += [item]
            self.update_widgets()
        else:
            item.setCheckState(Qt.Unchecked)
            QMessageBox.warning(self, "Attention", "Trop de séquences séléctionnées")

    def clear(self) -> None:
        """
        désélectionne toutes les sequences de liste des séquences
        """
        self.kill_calcul()
        try:
            self.denombrementWidget.calcul.kill()
        except AttributeError:
            pass

        self.comp = 0
        self.listesSequencesItem = []
        self.update_transcription()
        self.visualisationWidget.reinitialiser()
        self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.denombrementWidget), False)
        self.denombrementWidget.hide()
        self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.suitesUniquesRepeteesWidget), False)
        self.suitesUniquesRepeteesWidget.hide()
        self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.plusLongueSuiteWidget), False)
        self.plusLongueSuiteWidget.hide()
        self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.levenshteinWidget), False)
        self.levenshteinWidget.hide()
        self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.needlemanWunschWidget), False)
        self.needlemanWunschWidget.hide()
        self.tabWidget.setCurrentIndex(0)

    def effacer(self) -> None:
        """
        efface toute les sequences chargées jusqu'a maintenant
        """
        for liste in self.listesSequencesListWidget:
            liste.clear()
        self.clear()
        self.typeTabWidget.setCurrentIndex(0)

    def transcrire(self) -> None:
        """
        transciption de la sequence selectionnée, sauf si la séquence est une proteine
        """
        nom = self.listesSequencesItem[0].text()
        seq = self.listesSequencesItem[0].data(Qt.UserRole)
        boo = False
        if self.typeSelectionne == T_ADN:
            try:
                seqARN = adn_en_arn(seq)
                self.ajouter_item((nom, seqARN), T_ARN)
                listeProteine = arn_en_codons(seqARN)
                for i, proteine in enumerate(listeProteine):
                    nomProt = f"{nom}-P{i}"
                    self.ajouter_item((nomProt, proteine), T_PROTEINE)
                # self.ajouter_item((nom, listeProteine), T_PROTEINE)
            except ValueError as erreur:
                QMessageBox.warning(self, 'Erreur Transcription', f"{erreur}")
                boo = True
                print(f"erreur lors de la conversion, {erreur}")
        elif self.typeSelectionne == T_ARN:
            try:
                seqADN = arn_en_adn(seq)
                self.ajouter_item((nom, seqADN), T_ADN)
                listeProteine = arn_en_codons(seq)
                for i, proteine in enumerate(listeProteine):
                    nomProt = f"P{i}-{nom}"
                    self.ajouter_item((nomProt, proteine), T_PROTEINE)
            except ValueError as erreur:
                QMessageBox.warning(self, 'Erreur Transcription', f"{erreur}")
                boo = True
                print(f"erreur lors de la transcription, {erreur}")
        QMessageBox.information(self, 'Transcription', f"La transcription c'est terminée {'avec' if boo else 'sans'} erreurs")

    def selectionner_type(self, index : int) -> None:
        """
        appellé a chaque fois que l'utilisateur change d'onglet pour les types de séquence, défini l'attribut typeSelectionne au type correspondant
        :param index: numero de l'onglet ou l'on se trouve
        :type index: int
        """
        self.clear()
        if index == 0:
            self.typeSelectionne = T_ADN
        elif index == 1:
            self.typeSelectionne = T_ARN
        elif index == 2:
            self.typeSelectionne = T_PROTEINE
        self.update_transcription()
        self.listesSequencesListWidget[self.typeSelectionne].setCurrentRow(self.listesSequencesListWidget[self.typeSelectionne].count() - 1)
        self.update_visualisation(self.listesSequencesListWidget[self.typeSelectionne].currentItem(), None)

    def update_denombrement_widget(self) -> None:
        """
        update le widget correspondant à l'affichage de la fonction suite repetees
        """
        self.denombrementWidget.set_sequences(self.listesSequencesItem[0].text(),self.listesSequencesItem[0].data(Qt.UserRole))
        self.denombrementWidget.set_max(1)

    def update_suites_uniques_widget(self) -> None:
        """
        update le widget correspondant à l'affichage de la fonction suite repetees
        """
        self.suitesUniquesRepeteesWidget.set_sequences(self.listesSequencesItem[0].text(), self.listesSequencesItem[0].data(Qt.UserRole))
        self.suitesUniquesRepeteesWidget.set_longueur(1)

    def update_plus_longue_suite_widget(self) -> None:
        """
        update le widget correspondant à l'affichage de la fonction plus_longue_suite
        """
        self.plusLongueSuiteWidget.set_sequences(self.listesSequencesItem[0].text(), self.listesSequencesItem[0].data(Qt.UserRole))

    def update_levenshtein_widget(self) -> None:
        """
        update le widget correspondant à l'affichage de la fonction levenshtein et lance le calcul de la distance de levenshtein
        """
        self.levenshteinWidget.set_sequences(self.listesSequencesItem[0].text(),
                                             self.listesSequencesItem[0].data(Qt.UserRole),
                                             self.listesSequencesItem[1].text(),
                                             self.listesSequencesItem[1].data(Qt.UserRole))
        self.calculL = CalculLevenshtein(self.listesSequencesItem[0].data(Qt.UserRole),
                                     self.listesSequencesItem[1].data(Qt.UserRole))
        self.calculL.resultatSignal.connect(self.levenshteinWidget.set_resultat)
        self.calculL.start()

    def update_needleman_wunsch_widget(self) -> None:
        """
        update le widget correspondant à l'affichage de la fonction needleman_wunsch
        """
        self.needlemanWunschWidget.set_sequences(self.listesSequencesItem[0].data(Qt.UserRole),
                                                 self.listesSequencesItem[0].text(),
                                                 self.listesSequencesItem[1].data(Qt.UserRole),
                                                 self.listesSequencesItem[1].text())

    def update_visualisation(self, current : QListWidgetItem, _ : QListWidgetItem) -> None:
        """
        met a jour le widget visualisation
        :param current: séquence courrant
        :type current: QListWidgetItem
        :param _: séquence précédent
        :type _: QListWidgetItem
        """
        if current:
            self.visualisationWidget.set_sequence(current.text(), str(current.data(Qt.UserRole)))

    def update_widgets(self) -> None:
        """
        update l'ensemble des widgets nécessaire en fonction du nombre de séquences sélectionnées
        """
        if self.comp > 0:
            self.update_denombrement_widget()
            self.update_suites_uniques_widget()
            self.update_plus_longue_suite_widget()
            self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.denombrementWidget), True)
            self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.suitesUniquesRepeteesWidget), True)
            self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.plusLongueSuiteWidget), True)
            self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.levenshteinWidget), False)
            self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.needlemanWunschWidget), False)
        if self.comp == 2:
            if self.typeSelectionne in [T_ADN, T_ARN]:
                self.update_levenshtein_widget()
                self.update_needleman_wunsch_widget()
                self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.levenshteinWidget), True)
                self.tabWidget.setTabEnabled(self.tabWidget.indexOf(self.needlemanWunschWidget), True)
        self.update_transcription()
        self.tabWidget.setCurrentIndex(0)

    def update_transcription(self) -> None:
        """
        update le bouton transcription
        """
        self.buttonTranscription.setEnabled(False)
        if self.comp == 1 and self.typeSelectionne in [T_ADN, T_ARN]:
            self.buttonTranscription.setEnabled(True)

    def kill_calcul(self) -> None:
        """
        stop tout les calculs en cours et libère les ressources
        """
        try:
            self.calculL.kill()
        except AttributeError:
            pass

        kill_table_des_scores()

    def closeEvent(self, a0: QCloseEvent) -> None:
        """ reimplémentation de l'evenement de fermeture de la fenetre"""
        super().closeEvent(a0)
        self.quit()

    def quit(self) -> None:
        """ quitte l'application"""
        self.kill_calcul()
        try:
            self.denombrementWidget.calcul.kill()
        except AttributeError:
            pass
        qApp.quit()


if __name__ == '__main__':
    app = QApplication([])
    dark_stylesheet = qdarkstyle.load_stylesheet_pyqt5()
    app.setStyleSheet(dark_stylesheet)
    a = MainWindow()
    sys.exit(app.exec_())
