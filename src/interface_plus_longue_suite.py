""" fichier contenant les classes necessaires à l'affichage de levenshtein """
# pylint: disable=no-name-in-module

from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout, QPushButton, QLineEdit, QFormLayout #type: ignore
from PyQt5.QtCore import QThread, pyqtSignal, Qt #type: ignore

from src.stats import plus_longue_suite

class CalculPlusLongueSuite(QThread):
    """
    Exectute la fonction plus longue suite  dans un thread
    """
    resultatSignal = pyqtSignal(int, int)

    def __init__(self, sequence: str, sousSequence: str) -> None:
        super().__init__()
        print(sousSequence)
        self.sequence = sequence
        self.sousSequence = sousSequence

    def run(self) -> None:
        """
        Retourne le resultat des fonctions suites uniques et suites repetees sur la sequence self.sequence1
        """
        indiceDeb, nbRepet = plus_longue_suite(self.sequence, self.sousSequence)
        self.resultatSignal.emit(indiceDeb, nbRepet)

class PlusLongueSuite(QWidget):
    """
        widget d'affichage du résultat du calcul de la distance de levenshtein
        """

    def __init__(self) -> None:
        super().__init__()
        self.nomSeq = ""
        self.dataSeq = ""
        self.sousSeq = "AT"
        self.nbRepet = 0
        self.indiceDeb = -1
        self.calcul : QThread

        self.init_ui()

    def init_ui(self) -> None:
        """
        initialisation des éléments graphiques
        """
        self.titre = QLabel(f"Recherche de répététiton dans {self.nomSeq}")
        self.mainLabel = QLabel("Sélectionnez la sous-séquence")
        vlayout = QVBoxLayout()
        vlayout.addWidget(self.titre)
        vlayout.addWidget(self.mainLabel)

        self.bouton = QPushButton("Rechercher")
        self.bouton.clicked.connect(self.calculer)

        self.inputSeq = QLineEdit()
        self.inputSeq.setText("AT")
        self.inputSeq.setAlignment(Qt.AlignLeft)
        self.inputSeq.textChanged.connect(self.texte_change_input)
        form = QFormLayout()
        form.addRow("Sous-séquence", self.inputSeq)
        form.addRow(self.bouton)

        self.nbRepetLabel = QLabel("Nombre de répétition:")
        form.addRow(self.nbRepetLabel)
        vlayout.addWidget(self.nbRepetLabel)

        self.indiceDebLabel = QLabel("Indice de début :")
        form.addRow(self.indiceDebLabel)
        vlayout.addWidget(self.indiceDebLabel)

        formWidget = QWidget()
        formWidget.setLayout(form)
        vlayout.addWidget(formWidget)
        vlayout.addStretch()

        self.setLayout(vlayout)

    def calculer(self) -> None:
        """
        Lance le calcul de la séquence
        """
        self.mainLabel.setText("Calculs en cours")
        self.bouton.setEnabled(False)
        self.calcul = CalculPlusLongueSuite(self.dataSeq, self.sousSeq)
        self.calcul.start()
        self.calcul.resultatSignal.connect(self.set_resultat)

    def set_sequences(self, nomSeq: str, dataSeq :str) -> None:
        """
        modificateur des données des séquences
        :param nomSeq: nouveau nom de la séquence 1
        :type nomSeq: str
        :param dataSeq: nouvelle séquence 1
        :type dataSeq: str
        """
        self.bouton.setEnabled(True)
        self.nomSeq = nomSeq
        self.dataSeq = dataSeq
        self.titre.setText(f"Recherche de répététiton dans {self.nomSeq}")

    def set_resultat(self, indiceDeb: int, nbRepet : int) -> None:
        """
        modificateur du résultat
        :param nbRepet: nombre de répétition
        :type nbRepet: int
        :param indiceDeb: indice du 1er elements de la sous-séquence de répétition
        :type indiceDeb: int
        """
        self.nbRepet = nbRepet
        self.indiceDeb = indiceDeb
        self.nbRepetLabel.setText(f"Il y a {self.nbRepet} répétition de {self.sousSeq}")
        self.indiceDebLabel.setText(f"Indice de début : {self.indiceDeb}")
        self.bouton.setEnabled(True)
        self.mainLabel.setText("")

    def texte_change_input(self, texte: str) -> None:
        """
        Vérifie la valeur de la taille de séquenbce demandée par l'utilisateur
        """
        if texte != "":
            self.sousSeq = texte
