"""
Fonctions de statistiques descritives
"""
from typing import List, Dict, Tuple
from multiprocessing import Queue #type: ignore

def denombrement_n(sequence: str, longueur: int) -> Dict[str, int]:
    """
    Compte le nombre d'occurences de chaques sous-séquences de longueur donnée dans un dictionnaire

    :param sequence: la séquence à analyser
    :type sequence: string
    :param longueur: la longueur des sous-séquences à compter
    :type longueur: int
    :return: un dictionnaire contenant les fréquences d'apparition de chaque sous-séquence
    :rtype: Dict[str, int]
    """
    sortie: Dict[str, int] = {}
    for i in range(len(sequence)):
        if len(sequence) - i >= longueur:
            sousSeq: str = sequence[i:i + longueur]
            if sousSeq in sortie:
                sortie[sousSeq] += 1
            else:
                sortie[sousSeq] = 1
    return sortie

def denombrement(sequence: str, longueurSuiteMax: int) -> List[Dict[str, int]]:
    """
    Compte le nombre d'occurences de chaques sous-séquences de longueur 1,2,...,longueurSuiteMax triés par longueur dans des listes de dictionnaires

    :param sequence: la séquence à analyser
    :type sequence: string
    :param longueurSuiteMax: la longueur maximale des sous-séquences à compter
    :type longueurSuiteMax: int
    :return: un tableau de dictionnaires (triés par longueur des sous-séquences) contenant les fréquences d'apparition de chaque sous-séquence
    :rtype: List[Dict[str, int]]
    """
    if longueurSuiteMax < 0:
        raise ValueError('longueurSuiteMax incorrecte')
    sortie: List[Dict[str, int]] = [{} for _ in range(longueurSuiteMax)]
    for j in range(longueurSuiteMax):
        sortie[j] = denombrement_n(sequence, j)
    return sortie

def denombrement_process(queue : Queue, sequence : str, longueurSuiteMax : int) -> None:
    """
    ecrit le resultat de denombrement dans Queue
    :param queue: canal de communication entre les processus
    :type queue: Queue
    :param sequence: séquence
    :type sequence: str
    :param longueurSuiteMax: longueur maximun des sous-séquence
    :type longueurSuiteMax: int
    """
    resultat = denombrement(sequence, longueurSuiteMax)
    moyenne = []
    seqdiff = []
    for _, dic in enumerate(resultat):
        moyenne.append(sum([v for c,v in dic.items()])/len(dic))
        seqdiff.append(sum([1 for c,v in dic.items()]))
    queue.put((resultat, moyenne, seqdiff))


def plus_longue_suite(sequence: str, fragment: str) -> Tuple[int, int]:
    """
    Cherche la plus longue suite de `fragment` consécutifs dans la chaine `séquence`

    :param sequence: Séquence dans laquelle chercher la plus longue suite de `fragment`
    :type sequence: str
    :param fragment: Fragment à chercher dans `sequence`
    :type fragment: str
    :return: L'indice et la taille de la séquence cherchée
    :rtype: Tuple[int, int]
    """

    longSeq = len(sequence)
    longFrag = len(fragment)

    if longFrag == 0:
        return -1, 0

    maxIndice = -1
    maxLongueur = 0

    seqActuelleIndice = -1
    seqActuelleLongueur = 0

    dernierEstFrag = False

    indiceActuel = 0

    while indiceActuel <= longSeq - longFrag:

        if sequence[indiceActuel:indiceActuel + longFrag] == fragment:
            if not dernierEstFrag:
                dernierEstFrag = True
                seqActuelleIndice = indiceActuel

            seqActuelleLongueur += 1
            indiceActuel += longFrag

        else:
            indiceActuel += 1

            if dernierEstFrag:
                dernierEstFrag = False

                if seqActuelleLongueur > maxLongueur:
                    maxLongueur = seqActuelleLongueur
                    maxIndice = seqActuelleIndice

                seqActuelleIndice = -1
                seqActuelleLongueur = 0

    if seqActuelleLongueur > maxLongueur:
        maxLongueur = seqActuelleLongueur
        maxIndice = seqActuelleIndice

    return maxIndice, maxLongueur


def sequences_repetees(sequence: str, tailleSousSeq: int) -> Dict[str, int]:
    """
    Retourne toutes les sous-séquences de taille tailleSousSeq présentent plus d'une fois dans la séquence

    :param sequence: la séquence à analyser
    :type sequence: str
    :return: un dictionnaire contenant toutes les sous-séquences et leur nombre d'apparition
    :rtype: Dict[str, int]
    """
    sousSequence: Dict[str, int] = {}
    for cle, valeur in denombrement_n(sequence, tailleSousSeq).items():
        if valeur > 1:
            sousSequence[cle] = valeur
    return sousSequence

def sequences_uniques(sequence: str, tailleSousSeq: int) -> List[str]:
    """
    Retourne toutes les sous-séquences de taille tailleSousSeq une seule fois dans la séquence

    :param sequence: la séquence à analyser
    :type sequence: str
    :return: une liste contenant toutes les sous-séquences uniques
    :rtype: List[str]
    """
    sousSequence: List[str] = []
    for cle, valeur in denombrement_n(sequence, tailleSousSeq).items():
        if valeur == 1:
            sousSequence.append(cle)
    return sousSequence
