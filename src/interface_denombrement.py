""" fichier contenant les classes necessaires à l'affichage de denombrement """
# pylint: disable=no-name-in-module
import time
from typing import Dict, List
from multiprocessing import Process, Queue #type: ignore
import matplotlib #type: ignore

from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QVBoxLayout, QLineEdit, QFormLayout, QSizePolicy, QScrollArea #type: ignore
from PyQt5.QtGui import QIntValidator #type: ignore
from PyQt5.QtCore import Qt, QThread, pyqtSignal #type: ignore

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas # type: ignore
from matplotlib.figure import Figure # type: ignore

from src.stats import denombrement_process

matplotlib.use('Qt5Agg')

class CalculDenombrement(QThread):
    """
    Execute la fonction dénombrement dans un thread
    """
    resultatSignal = pyqtSignal(list,list,list)

    def __init__(self, sequence1: str, maxN: int):
        super().__init__()
        self.sequence1 = sequence1
        self.maxN = maxN
        self.queue : Queue = Queue()
        self.process = Process(target=denombrement_process, args=(self.queue, self.sequence1, self.maxN))
        self.enCours = False

    def run(self) -> None:
        """
        Retourne le resultat de la fonction dénombrement sur la sequence self.sequence1
        """
        self.process.start()
        self.enCours = True
        while self.enCours:
            resultat, moyennes, seqdiff = self.queue.get()
            if resultat:
                self.resultatSignal.emit(resultat, moyennes, seqdiff)
                self.enCours = False

    def kill(self) -> None:
        """
        stop le calcul et libère les ressources
        """
        if self.enCours:
            self.enCours = False
            self.process.kill()
            # print(self.process.pid)
            time.sleep(0.1)
            self.process.close()



class DenombrementWidget(QWidget):
    """
    Widget pour l'affichage des resultats de la fonction denombrement
    """
    def __init__(self) -> None:
        super().__init__()
        self.nomSeq1 = ""
        self.dataSeq1 = ""
        self.resultat : List[Dict[str, int]]
        self.maxN = 1
        self.calcul: CalculDenombrement

        self.init_ui()

    def init_ui(self) -> None:
        """
        Définition de l'interface du plugin
        """
        self.titre = QLabel(f"Répétitions de sous-séquences dans la séquence {self.nomSeq1} (longueur : {len(self.dataSeq1)})")
        self.mainLabel = QLabel("Sélectionnez la taille des sous-séquences à analyser")
        vlayout = QVBoxLayout()
        vlayout.addWidget(self.titre)
        vlayout.addWidget(self.mainLabel)

        self.bouton = QPushButton("Calculer")
        self.bouton.clicked.connect(self.calculer)

        self.inputNombre = QLineEdit()
        self.inputNombre.setValidator(QIntValidator(bottom=1))
        self.inputNombre.setText("1")
        self.inputNombre.setAlignment(Qt.AlignLeft)
        self.inputNombre.textChanged.connect(self.texte_change_input)
        form = QFormLayout()
        form.addRow("Longueur des sous-séquences", self.inputNombre)
        form.addRow(self.bouton)
        vlayout.addLayout(form)

        self.graphiqueMoyennes = PlotCanvas()
        self.graphiqueMoyennes.axes.set_title("Nombre moyen de répétition d'une sous-séquence de longueur donnée")
        self.graphiqueMoyennes.axes.set_ylabel("Répétitions")
        self.graphiqueMoyennes.axes.set_xlabel("Longueur des sous-séquences")

        self.graphiqueSeqdiff = PlotCanvas()
        self.graphiqueSeqdiff.axes.set_title("Nombre de séquences dfférentes pour une longueur donnée")
        self.graphiqueSeqdiff.axes.set_ylabel("Nombre de séquences différentes")
        self.graphiqueSeqdiff.axes.set_xlabel("Longueur des sous-séquences")

        graphLayout = QVBoxLayout()
        graphLayout.addWidget(self.graphiqueMoyennes)
        graphLayout.addWidget(self.graphiqueSeqdiff)
        qScrollArea = QScrollArea()
        qScrollArea.setLayout(graphLayout)
        vlayout.addWidget(qScrollArea)
        self.setLayout(vlayout)

    def calculer(self) -> None:
        """
        Lance le calcul de la séquence
        """
        self.mainLabel.setText("Calculs de dénombrement en cours")
        self.bouton.setEnabled(False)
        self.calcul = CalculDenombrement(self.dataSeq1, self.maxN)
        self.calcul.start()
        self.calcul.resultatSignal.connect(self.set_resultat)


    def set_sequences(self, nomSeq1 : str, dataSeq1 : str) -> None:
        """
        Changer la séquence
        :param nomSeq1: nom de la séquence
        :type nomSeq1: str
        :param dataSeq1: séquence
        :type dataSeq1: str
        """
        self.bouton.setEnabled(True)
        self.nomSeq1 = nomSeq1
        self.dataSeq1 = dataSeq1
        self.titre.setText(f"Répétitions de sous-séquences dans la séquence {self.nomSeq1} (longueur : {len(self.dataSeq1)})")
        self.inputNombre.setValidator(QIntValidator(bottom=1, top=len(self.dataSeq1)))

    def texte_change_input(self, texte: str) -> None:
        """
        Vérifie la valeur de la taille de séquenbce demandée par l'utilisateur
        :param texte: nouvelle taille
        :type texte: str
        """
        if texte != "":
            self.set_max(int(texte)+1)

    def set_max(self, maxN: int) -> None:
        """
        Changer la taille des sous-séquences à dénombrer
        :param maxN: taille de la sous-séquence
        :type maxN: int
        """
        self.maxN = maxN

    def set_resultat(self, resultatsDenombrement: List[Dict[str, int]], moyenne: List[int], seqdiff: List[int]) -> None:
        """
        Afficher les résultats des calculs
        :param resultatsDenombrement: liste des dictionnaires contenant les séquences dénombrées par taille
        :type resultatsDenombrement: List[Dict[str, int]]
        :param moyenne: liste des moyennes
        :type moyenne: List[int]
        :param seqdiff: liste du nombre de séquences differentes selon la taille
        :type seqdiff: List[int]
        """
        self.resultat = resultatsDenombrement
        self.graphiqueMoyennes.axes.cla()
        self.graphiqueMoyennes.axes.bar([i+1 for i in range(len(moyenne[1:]))],moyenne[1:])
        self.graphiqueMoyennes.axes.set_title("Nombre moyen de répétition d'une sous-séquence de longueur donnée")
        self.graphiqueMoyennes.axes.set_ylabel("Répétitions")
        self.graphiqueMoyennes.axes.set_xlabel("Longueur des sous-séquences")
        self.graphiqueMoyennes.draw()
        self.graphiqueSeqdiff.axes.cla()
        self.graphiqueSeqdiff.axes.bar([i+1 for i in range(len(seqdiff[1:]))],seqdiff[1:])
        self.graphiqueSeqdiff.axes.set_title("Nombre de séquences dfférentes pour une longueur donnée")
        self.graphiqueSeqdiff.axes.set_ylabel("Nombre de séquences différentes")
        self.graphiqueSeqdiff.axes.set_xlabel("Longueur des sous-séquences")
        self.graphiqueSeqdiff.draw()
        self.bouton.setEnabled(True)
        self.mainLabel.setText("Nombre moyen de répétitions en fonction des longueurs de sous séquences")

class PlotCanvas(FigureCanvas):
    """classe d'intégration de matplotlib à Qt"""
    def __init__(self, width=5, height=4, dpi=100)  -> None:
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super().__init__(fig)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
