#pylint: disable=unsubscriptable-object, unnecessary-comprehension, global-statement
"""
Fichier fonctions d'application de l'algorithme de Needleman-Wunsch
"""

from typing import List, Tuple, Generator, Dict, Union, Optional
from os import system, fork, pipe, close, _exit, write

import numpy as np # type: ignore

STOP = False

def kill_table_des_scores()  -> None:
    """
    Arrête le calcul de la table des scores
    """
    global STOP
    STOP = True

def table_des_scores_initial(motA: str,
                     motB: str,
                     similarites: List[List[int]],
                     penaliteTrou: int = -3) -> List[List[int]]:
    """
    Fonction de calcul des scores d'alignement entre motA et motB avec l'algorithme de Needleman-Wunsch

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: La matrice des scores
    :rtype: List[List[int]]
    """

    global STOP

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}
    STOP = False

    # Calcul préalable des longueurs des mots
    longA = len(motA)
    longB = len(motB)

    # Initialisation de la matrice des scores
    scores = [[penaliteTrou * k for k in range(longA + 1)]] + [[penaliteTrou * i] + [0] * (longA) for i in range(1, longB + 1)]

    for lig in range(1, longB + 1):  # lignes
        for col in range(1, longA + 1):  # colonnes
            if STOP:
                raise RuntimeError("Fin du processus")
            try:
                indiceA = tableTransfert[motA[col - 1]]
            except KeyError:
                indiceA = tableTransfert['N']
            try:
                indiceB = tableTransfert[motB[lig - 1]]
            except KeyError:
                indiceB = tableTransfert['N']
            score = similarites[indiceA][indiceB]

            scores[lig][col] = max(
                scores[lig - 1][col - 1] +
                score,
                scores[lig - 1][col] + penaliteTrou,
                scores[lig][col - 1] + penaliteTrou)
            # On calcul les scores sur le principe d'optimalité

    return scores


def table_des_scores_np_v1(motA: str,
                     motB: str,
                     similarites: List[List[int]],
                     penaliteTrou: int = -3) -> List[List[int]]:
    """
    Fonction de calcul des scores d'alignement entre motA et motB avec l'algorithme de Needleman-Wunsch

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: La matrice des scores
    :rtype: List[List[int]]
    """

    global STOP

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}
    STOP = False

    # Calcul préalable des longueurs des mots
    longA = len(motA)
    longB = len(motB)

    # Initialisation de la matrice des scores
    scores = np.array([[penaliteTrou * k for k in range(longA + 1)]] + [[penaliteTrou * i] + [0] * (longA) for i in range(1, longB + 1)], dtype=np.int32)

    for lig in range(1, longB + 1):  # lignes
        for col in range(1, longA + 1):  # colonnes
            if STOP:
                raise RuntimeError("Fin du processus")
            try:
                indiceA = tableTransfert[motA[col - 1]]
            except KeyError:
                indiceA = tableTransfert['N']
            try:
                indiceB = tableTransfert[motB[lig - 1]]
            except KeyError:
                indiceB = tableTransfert['N']
            score = similarites[indiceA][indiceB]

            scores[lig, col] = max(
                scores[lig - 1, col - 1] +
                score,
                scores[lig - 1, col] + penaliteTrou,
                scores[lig, col - 1] + penaliteTrou)
            # On calcul les scores sur le principe d'optimalité

    return scores


def table_des_scores(motA: str,
                     motB: str,
                     similarites: List[List[int]],
                     penaliteTrou: int = -3) -> List[List[int]]:
    """
    Fonction de calcul des scores d'alignement entre motA et motB avec l'algorithme de Needleman-Wunsch

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: La matrice des scores
    :rtype: List[List[int]]
    """

    global STOP

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}
    STOP = False

    # Calcul préalable des longueurs des mots
    longA = len(motA)
    longB = len(motB)

    # Initialisation de la matrice des scores
    scores = np.zeros((longB + 1, longA + 1), dtype=np.int32)
    lignePrec = [penaliteTrou * k for k in range(longA + 1)]

    for lig in range(1, longB + 1):  # lignes
        ligne = [penaliteTrou * lig] + [0] * (longA)
        for col in range(1, longA + 1):  # colonnes
            if STOP:
                raise RuntimeError("Fin du processus")
            try:
                indiceA = tableTransfert[motA[col - 1]]
            except KeyError:
                indiceA = tableTransfert['N']
            try:
                indiceB = tableTransfert[motB[lig - 1]]
            except KeyError:
                indiceB = tableTransfert['N']
            score = similarites[indiceA][indiceB]
            ligne[col] = max(
                lignePrec[col - 1] +
                score,
                lignePrec[col] + penaliteTrou,
                ligne[col - 1] + penaliteTrou)
            # On calcul les scores sur le principe d'optimalité

        scores[lig - 1] = lignePrec
        lignePrec = ligne

    scores[longB] = lignePrec

    return scores


def alignements_optimaux_rec_simple(motA: str,
                                    motB: str,
                                    table: List[List[int]] = None,
                                    similarites: List[List[int]] = None,
                                    penaliteTrou: int = -3) -> List[Tuple[str, str]]:
    """
    Fonction de calcul des scores d'alignements optimaux entre motA et motB avec l'algorithme de Needleman-Wunsch de manière récursive

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: Liste des couples des alignements optimaux
    :rtype: List[Tuple[int]]
    """
    if not motA or not motB:
        return [("-" * len(motB) + motA, "-" * len(motA) + motB)]

    # Matrice de similarité dite de BLAST utilisée par défaut
    if not similarites or \
       not (len(similarites) == len(similarites[0]) and len(similarites) == 5):
        similarites = [[1 if i == j else -2 if i == 4 or j == 4 else -3 for j in range(5)]
                                                                        for i in range(5)]

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}

    if table is None:
        table = table_des_scores(motA, motB, similarites, penaliteTrou)

    ####### FONCTION RECURSIVE #######

    def parcours_table(lig, col):
        if lig == col == 0:
            return [("", "")]
        listeOpti = []

        # Valeur de la case actuelle
        actuel = table[lig][col]

        indiceA = tableTransfert[motA[col - 1]]
        indiceB = tableTransfert[motB[lig - 1]]
        score = similarites[indiceA][indiceB]

        # Pour chaques cases adjacentes, on regarde si la différence entre les
        # 2 cases est la bonne, et si oui alors on calcul les mots optimaux correspondants
        # à la case de destination. On vérifie également les cas limites ( col == 0 xor lig == 0)
        # Le mot B est décalé d'un trou
        if col != 0 and actuel - table[lig][col - 1] == penaliteTrou:
            ret = parcours_table(lig, col - 1)
            listeOpti += [(a + motA[col - 1], b + '-') for a, b in ret]

        # Le mot A est décalé d'un trou
        if lig != 0 and actuel - table[lig - 1][col] == penaliteTrou:
            ret = parcours_table(lig - 1, col)
            listeOpti += [(a + '-', b + motB[lig - 1]) for a, b in ret]

        # Mots alignés
        if lig*col != 0 and actuel - table[lig - 1][col - 1] == score:
            ret = parcours_table(lig - 1, col - 1)
            listeOpti += [(a + motA[col - 1], b + motB[lig - 1])
                          for a, b in ret]

        return listeOpti

    return parcours_table(len(motB), len(motA))


def alignements_optimaux_rec_multiprocess(motA: str,
                                          motB: str,
                                          table: List[List[int]] = None,
                                          similarites: List[List[int]] = None,
                                          penaliteTrou: int = -3) -> List[Tuple[str, str]]:
    """
    Fonction de calcul des scores d'alignements optimaux entre motA et motB avec l'algorithme de Needleman-Wunsch de manière récursive

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: Liste des couples des alignements optimaux
    :rtype: List[Tuple[int]]
    """
    if not motA or not motB:
        return [("-" * len(motB) + motA, "-" * len(motA) + motB)]

    # Matrice de similarité dite de BLAST utilisée par défaut
    if not similarites or \
       not (len(similarites) == len(similarites[0]) and len(similarites) == 5):
        similarites = [[1 if i == j else -2 if i == 4 or j == 4 else -3 for j in range(5)]
                                                                        for i in range(5)]

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}

    if table is None:
        table = table_des_scores(motA, motB, similarites, penaliteTrou)

    ####### FONCTION RECURSIVE #######

    def parcours_table(lig, col, writeFd, alignementA="", alignementB=""):
        if lig == col == 0:
            write(writeFd, f"{alignementA} {alignementB}\n".encode("ascii"))
            _exit(0)
        # Valeur de la case actuelle
        actuel = table[lig][col]

        indiceA = tableTransfert[motA[col - 1]]
        indiceB = tableTransfert[motB[lig - 1]]
        score = similarites[indiceA][indiceB]

        aFaire = []

        # Pour chaques cases adjacentes, on regarde si la différence entre les
        # 2 cases est la bonne, et si oui alors on calcul les mots optimaux correspondants
        # à la case de destination. On vérifie également les cas limites ( col == 0 xor lig == 0)
        # Le mot B est décalé d'un trou
        if col != 0 and actuel - table[lig][col - 1] == penaliteTrou:
            aFaire.append(lambda: parcours_table(lig, col - 1, writeFd, motA[col - 1] + alignementA, '-' + alignementB))

        # Le mot A est décalé d'un trou
        if lig != 0 and actuel - table[lig - 1][col] == penaliteTrou:
            aFaire.append(lambda: parcours_table(lig - 1, col, writeFd, '-' + alignementA, motB[lig - 1] + alignementB))

        # Mots alignés
        if lig*col != 0 and actuel - table[lig - 1][col - 1] == score:
            aFaire.append(lambda: parcours_table(lig - 1, col - 1, writeFd, motA[col - 1] + alignementA, motB[lig - 1] + alignementB))

        for fonction in aFaire[1:]:
            write(writeFd, b"fork\n")
            if fork():
                fonction()

        aFaire[0]()



    read, writeFd = pipe()

    processId = fork()
    alignements: List[Tuple[str, str]] = []

    if processId:
        # Processus parent (listener)
        close(writeFd)

        processusActifs = 1

        with open(read, "r") as pipeReader:
            while processusActifs:
                ligne = pipeReader.readline()

                if ligne=="fork\n":
                    processusActifs += 1
                else:
                    aligA, aligB = ligne.split()
                    alignements.append((aligA, aligB))
                    processusActifs -= 1


    else:
        close(read)
        parcours_table(len(motB), len(motA), writeFd)

    return alignements


def alignements_optimaux_rec_sauvegardes(motA: str,
                                         motB: str,
                                         table: List[List[int]] = None,
                                         similarites: List[List[int]] = None,
                                         penaliteTrou: int = -3) -> List[Tuple[str, str]]:
    """
    Fonction de calcul des scores d'alignements optimaux entre motA et motB avec l'algorithme de Needleman-Wunsch de manière récursive

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité
    :type similarites: List[List[int]]
    :param penaliteTrou: la pénalité de trou
    :type penaliteTrou: int
    :return: Liste des couples des alignements optimaux
    :rtype: List[Tuple[int]]
    """
    if not motA or not motB:
        return [("-" * len(motB) + motA, "-" * len(motA) + motB)]

    # Matrice de similarité dite de BLAST utilisée par défaut
    if not similarites or \
       not (len(similarites) == len(similarites[0]) and len(similarites) == 5):
        similarites = [[1 if i == j else -2 if i == 4 or j == 4 else -3 for j in range(5)]
                                                                        for i in range(5)]

    tableTransfert = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}

    if table is None:
        table = table_des_scores(motA, motB, similarites, penaliteTrou)

    # Dictionnaire contenant toutes les cases déjà calculées
    # pour ne pas calculer plusieurs fois la même
    dejaFais = {(0, 0): [("", "")]}

    ####### FONCTION RECURSIVE #######

    def parcours_table(lig, col):
        try:
            # Si la case a déjà été calculée, on retourne le résultat connu
            # Cas de base : col == lig == 0
            return dejaFais[(lig, col)]
        except KeyError:
            listeOpti = []

            # Valeur de la case actuelle
            actuel = table[lig][col]

            indiceA = tableTransfert[motA[col - 1]]
            indiceB = tableTransfert[motB[lig - 1]]
            score = similarites[indiceA][indiceB]

            # Pour chaques cases adjacentes, on regarde si la différence entre les
            # 2 cases est la bonne, et si oui alors on calcul les mots optimaux correspondants
            # à la case de destination. On vérifie également les cas limites ( col == 0 xor lig == 0)
            # Le mot B est décalé d'un trou
            if col != 0 and actuel - table[lig][col - 1] == penaliteTrou:
                ret = parcours_table(lig, col - 1)
                listeOpti += [(a + motA[col - 1], b + '-') for a, b in ret]

            # Le mot A est décalé d'un trou
            if lig != 0 and actuel - table[lig - 1][col] == penaliteTrou:
                ret = parcours_table(lig - 1, col)
                listeOpti += [(a + '-', b + motB[lig - 1]) for a, b in ret]

            # Mots alignés
            if lig*col != 0 and actuel - table[lig - 1][col - 1] == score:
                ret = parcours_table(lig - 1, col - 1)
                listeOpti += [(a + motA[col - 1], b + motB[lig - 1])
                              for a, b in ret]

            # On enregistre les résultats de la case actuelle pour éviter d'avoir trop de calculs
            dejaFais[(lig, col)] = listeOpti

            return listeOpti

    return parcours_table(len(motB), len(motA))


def alignements_optimaux(
    motA: str,
    motB: str,
    table: List[List[int]] = None,
    similaritesEtTrou: Tuple[Union[List[List[int]], None], int] = (None, 0),
    debug: bool = False,
    precedant: bool = False,
    loop: bool = False
) -> Generator[Tuple[Tuple[str, str], List[List[int]], List[Tuple[int, int]], int], Optional[bool], None]:

    """
    Fonction de calcul des scores d'alignements optimaux entre motA et motB avec l'algorithme de Needleman-Wunsch

    :param motA: une suite de caractères
    :type motA: str
    :param motB: une suite de caractères
    :type motB: str
    :param similarites: Une matrice de similarité suivi de la pénalité de trou
    :type similarites: Tuple[List[List[int]], int]
    :param debug: Si `True`, affiche l'état de la pile lors de l'execution de la fonction
    :type debug: bool
    :return: Yield les alignements optimaux
    :rtype: Generator[Tuple[str, str], Optional[bool], None]
    """

    tableTransfert: Dict[str, int] = {'A': 0, 'G': 1, 'C': 2, 'T': 3, 'N': 4}


    # On récupère la valeur de la pénalité de trou
    penaliteTrou = similaritesEtTrou[1]
    # On récupère la table des similatités séparée
    similarites = similaritesEtTrou[0]

    # Création matrice similarites si elle n'est pas définie

    if not similarites or \
        not (len(similarites) == len(similarites[0]) and len(similarites) == 5):
        similarites = [[1 if i == j else -2 if i == 4 or j == 4 else -3 for j in range(5)]
                       for i in range(5)]
        penaliteTrou = -3

    # Récupération de la table des scores
    if table is None:
        table = table_des_scores(motA, motB, similarites, penaliteTrou)

    index: int = 0

    if not motA or not motB:
        yield ("-" * len(motB) + motA, "-" * len(motA) + motB), \
              table, \
              [(0,i) if motB else (i, 0) for i in range(len(motA+motB))], \
              index
        return None

    if debug:
        print(*table, sep="\n")

    # Pile contenant toutes les cases à traiter
    # Sous la forme [ ligne, colonne, direction dans laquelle on s'est deplacé, indice de la case precedante dans la pile, liste de boolean indiquant les directions déjà traitées ]
    aFaire: List[Tuple[int, int, int, int, List[bool]]] = [
            (len(motB), len(motA), -1, -1, [precedant] * 3)
    ]

    # [motA, motB] -> alignement
    alignementActuel = ("", "")

    # Tant qu'il y a des cas à traiter
    while aFaire or loop:
        if not aFaire:
            aFaire = [(len(motB), len(motA), -1, -1, [not precedant]*3)]
            precedant = not precedant
            index += 1 if precedant else -1

        # On traite la case tout en haut de la pile
        lig, col, direction, indicePrecedant, directionDejaFaites = aFaire[-1]

        # Indice de la case actuellement traitée dans la pile
        indiceActuel = len(aFaire) - 1

        actuel = table[lig][col]

        # On actualise alignementActuel

        if direction != -1:
            alignementActuel = (('-' if direction == 1 else motA[col]) + alignementActuel[0],
                                (motB[lig] if direction > 0 else '-') + alignementActuel[1])

            aFaire[indicePrecedant][-1][direction] = not precedant

        if debug:
            input()
            system("clear")
            print(*aFaire, sep="\n")
            print(alignementActuel)

        try:
            indiceA = tableTransfert[motA[col - 1]]
        except KeyError:
            indiceA = tableTransfert["N"]

        try:
            indiceB = tableTransfert[motB[lig - 1]]
        except KeyError:
            indiceB = tableTransfert["N"]
        score = similarites[indiceA][indiceB]

        execute_pile((precedant,
                      directionDejaFaites,
                      score, lig, col,
                      indiceActuel,
                      actuel, table,
                      penaliteTrou, aFaire))

        if indiceActuel == len(aFaire)-1:
            # Si on est à la case en haut à gauche on yield
            if lig == col == 0:
                index += 1 if not precedant else -1
                prochainPrecedant = yield alignementActuel,\
                      table,\
                      [(i,j) for i, j, *_ in aFaire], \
                      index

                if prochainPrecedant is None:
                    prochainPrecedant = False

                if prochainPrecedant != precedant:
                    aFaire[indicePrecedant][-1][direction] ^= True

                precedant = prochainPrecedant

                if debug:
                    print("Precedant :", precedant)

            # On pop la dernière case
            aFaire.pop()
            alignementActuel = (alignementActuel[0][2:],
                                alignementActuel[1][2:])

def execute_pile(args: Tuple[bool, List[bool], int, int, int, int, int, List[List[int]], int, List[Tuple[int, int, int, int, List[bool]]]]) -> None:
    """
    Ajoute à la pile la prochaine case à parcourir

    :param args: Tuple contenant les élements dont la fonction à besoin: precedant, directionDejaFaites, score, lig, col, indiceActuel, actuel, table, penaliteTrou et aFaire
    :type args: Tuple[bool, List[bool], int, int, int, int, int, List[List[int]], int, List[Tuple[int, int, int, int, List[bool]]]]
    :rtype: None
    """

    precedant, directionDejaFaites, score, lig, col, indiceActuel, actuel, table, penaliteTrou, aFaire = args

    if not precedant:

        # Pour chaque case adjacente, si elle doit être traitée on l'ajoute à la pile
        if not directionDejaFaites[0] and col > 0 and \
                            actuel - table[lig][col - 1] == penaliteTrou:
            aFaire.append((lig, col - 1, 0, indiceActuel, [False] * 3))

        elif not directionDejaFaites[1] and lig > 0 and \
                            actuel - table[lig - 1][col] == penaliteTrou:
            aFaire.append((lig - 1, col, 1, indiceActuel, [False] * 3))

        elif not directionDejaFaites[2] and lig > 0 and col > 0 and \
                            actuel - table[lig - 1][col - 1] == score:
            aFaire.append((lig - 1, col - 1, 2, indiceActuel, [False] * 3))

    else:
        # Pour chaque case adjacente, si elle doit être traitée on l'ajoute à la pile
        if directionDejaFaites[2] and lig > 0 and col > 0 and \
                            actuel - table[lig - 1][col - 1] == score:
            aFaire.append((lig - 1, col - 1, 2, indiceActuel, [True] * 3))

        elif directionDejaFaites[1] and lig > 0 and \
                            actuel - table[lig - 1][col] == penaliteTrou:
            aFaire.append((lig - 1, col, 1, indiceActuel, [True] * 3))

        elif directionDejaFaites[0] and col > 0 and \
                            actuel - table[lig][col - 1] == penaliteTrou:
            aFaire.append((lig, col - 1, 0, indiceActuel, [True] * 3))
