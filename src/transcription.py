"""
Algorithmes de transcription
"""
from typing import Dict, Tuple, List

# type
T_ADN = 0
T_ARN = 1
T_PROTEINE = 2

def adn_en_arn(sequence: str) -> str:
    """
    Fonction de transcription d'une séquence d'ADN en ARN

    :param sequence: la séquence d'ADN à transcrire
    :type sequence: str
    :raises ValueError: la séquence d'ADN contient des bases autres que A,C,G,T,R,Y,K,M,S,W,B,D,H,V,N,X ou -
    :return: une chaîne représentant la transcription en suite d'ARN de la séquence d'entrée
    :rtype: str
    """
    tableConversion: Dict[str,str] = {
        "T": "A", "C": "G", "G": "C", "A": "U",
        "Y": "R", "R": "Y", "K": "M", "M": "K",
        "S": "S", "W": "W", "B": "V", "D": "H",
        "H": "D", "V": "B", "N": "N", "X": "X",
        "-": "-"
    }
    sortie: str = ""
    for base in sequence.upper():
        try:
            sortie += tableConversion[base]
        except KeyError:
            raise ValueError("La séquence d'ADN contient des bases incorrectes") from KeyError
    return sortie

def arn_en_adn(sequence: str) -> str:
    """
    Fonction de transcription d'une séquence d'ARN en ADN

    :param sequence: la séquence d'ARN à transcrire
    :type sequence: str
    :raises ValueError: la séquence d'ARN contient des bases autres que A,C,G,U,R,Y,K,M,S,W,B,D,H,V,N,X ou -
    :return: une chaîne représentant la transcription en suite d'ARN de la séquence d'entrée
    :rtype: str
    """
    tableConversion: Dict[str,str] = {
        "A": "T", "C": "G", "G": "C", "U": "A",
        "Y": "R", "R": "Y", "K": "M", "M": "K",
        "S": "S", "W": "W", "V": "B", "H": "D",
        "D": "H", "B": "V", "N": "N", "X": "X",
        "-": "-"
    }
    sortie: str = ""
    for base in sequence:
        try:
            sortie += tableConversion[base]
        except KeyError:
            raise ValueError("La séquence d'ARN contient des bases incorrectes") from KeyError
    return sortie

def arn_en_codons(sequence: str) -> List[str]:
    """
    Fonction de transcription d'une séquence d'ARN à l'aide des codons
    Retourne une liste des chaînes polypeptidiques synthétisées par arn messager donnée en paramètre

    :param sequence: la séquence d'ARN à transcrire
    :type sequence: str
    :raises ValueError: la longueur de la séquence d'ARN n'est pas un multiple de 3 ou contient des bases autres que A,C,G ou U
    :return: une chaîne représentant la transcription en suite de codons de la séquence d'entrée
    :rtype: str
    """
    tableConversion: Dict[str,str] = {
        "UUU": "F", "UUC": "F", "UUA": "L", "UUG": "L",
        "UCU": "S", "UCC": "S", "UCA": "S", "UCG": "S",
        "UAU": "Y", "UAC": "Y", "UAA": "*", "UAG": "*",
        "UGU": "C", "UGC": "C", "UGA": "*", "UGG": "W",
        "CUU": "L", "CUC": "L", "CUA": "L", "CUG": "L",
        "CCU": "P", "CCC": "P", "CCA": "P", "CCG": "P",
        "CAU": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
        "CGU": "R", "CGC": "R", "CGA": "R", "CGG": "R",
        "AUU": "I", "AUC": "I", "AUA": "I", "AUG": "M",
        "ACU": "T", "ACC": "T", "ACA": "T", "ACG": "T",
        "AAU": "N", "AAC": "N", "AAA": "K", "AAG": "K",
        "AGU": "S", "AGC": "S", "AGA": "R", "AGG": "R",
        "GUU": "V", "GUC": "V", "GUA": "V", "GUG": "V",
        "GCU": "A", "GCC": "A", "GCA": "A", "GCG": "A",
        "GAU": "D", "GAC": "D", "GAA": "E", "GAG": "E",
        "GGU": "G", "GGC": "G", "GGA": "G", "GGG": "G"
    }
    if len(sequence)%3 != 0:
        raise ValueError("La longueur de la séquence d'ARN est incorrecte")
    sortie: List[str] = []
    chainePolypeptidique = ""
    for i in range(0,len(sequence),3):
        try:
            char = tableConversion[sequence[i:i+3]]
            if char == "*":
                sortie.append(chainePolypeptidique)
                chainePolypeptidique = ""
            else:
                chainePolypeptidique += char
        except KeyError:
            for base in sequence[i:i+3]:
                if base not in "ACGURYKMSWBDHVNX-":
                    raise ValueError("La séquence d'ARN contient des bases incorrectes") from KeyError
            # Si la base fait partie des bases possibles d'une séquence mais que le codon corresppndant n'existe pas on ajoute un codon joker
            chainePolypeptidique += "X"
    sortie.append(chainePolypeptidique)
    return sortie

def codons_en_acides_amines(sequence: str, formeReduite: bool = True) -> List[str]:
    """
    Retourne la liste des noms des acides aminés correspondant leurs codes

    :param sequence: la séquence de codons à transcrire en acides aminés
    :type sequence: str
    :raises ValueError: la chaîne contient des condons invalides
    :return: une liste de chaînes contenant les noms des acides aminés
    :rtype: List[str]
    """
    tableConversion: Dict[str,Tuple[str,str]] = {
        "F": ("Phe", "Phénylalanine"),
        "L": ("Leu", "Leucine"),
        "I": ("Ile", "Isoleucine"),
        "M": ("Met", "Méthionine"),
        "V": ("Val", "Valine"),
        "S": ("Ser", "Sérine"),
        "P": ("Pro", "Proline"),
        "T": ("Thr", "Thréonine"),
        "A": ("Ala", "Alanine"),
        "Y": ("Tyr", "Tyrosine"),
        "H": ("His", "Histidine"),
        "Q": ("Gln", "Glutamine"),
        "N": ("Asn", "Asparagine"),
        "K": ("Lys", "Lysine"),
        "D": ("Asp", "Acide aspartique"),
        "E": ("Glu", "Acide glutamique"),
        "C": ("Cys", "Cystéine"),
        "W": ("Trp", "Tryptophane"),
        "R": ("Arg", "Arginine"),
        "G": ("Gly", "Glycine"),
        "X": ("Inc", "Inconnu"),
        "*": ("STOP","STOP")
        }
    sortie: List[str] = []
    for base in sequence.upper():
        try:
            if formeReduite:
                sortie.append(tableConversion[base][0])
            else:
                sortie.append(tableConversion[base][1])
        except KeyError:
            raise ValueError("La séquence contient des codons incorrects") from KeyError
    return sortie

def verif(sequence : str, typeSeq : int) -> bool:
    """
        verifie si la chaine de caractére correspond bien à une séquence du type donné
        :param sequence: sequence d'ADN a vérifier
        :type sequence: str
        :param typeSeq: constante de type
        :type typeSeq: int
        :return: un booléen vrai si la séquence est correcte , faux sinon
        :rtype: bool
        """
    table = ""
    if typeSeq == T_ADN:
        table = "ACGTRYKMSWBDHVNX-"
    elif typeSeq == T_ARN:
        table = "ACGURYKMSWBDHVNX-"
    elif typeSeq == T_PROTEINE:
        table = "FLIMVSPTAYHQNKDECWRGX*"
    for base in sequence:
        if base not in table:
            return False
    return True
