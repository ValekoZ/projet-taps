""" fichier contenant les classes necessaires à l'affichage de levenshtein """
# pylint: disable=no-name-in-module
import time
from multiprocessing import Process, Queue #type: ignore

from PyQt5.QtWidgets import QWidget, QLabel, QVBoxLayout #type: ignore
from PyQt5.QtCore import QThread, pyqtSignal #type: ignore

from src.levenshtein import distance_process

class CalculLevenshtein(QThread):
    """
    thread d'ecoute pour le calcul de la distance de levenshtein dans un autre processus
    """
    resultatSignal = pyqtSignal(int)

    def __init__(self, seqence1 : str, sequence2 : str) -> None:
        super().__init__()
        self.sequence1 = seqence1
        self.sequence2 = sequence2
        self.queue : Queue = Queue()
        self.process = Process(target=distance_process, args=(self.queue, self.sequence1, self.sequence2))
        self.enCours = False

    def run(self) -> None:
        """
        demarre le calcul de la distance de levenshtein dans un nouveau processus
        """
        self.process.start()
        self.enCours = True
        while self.enCours:
            resultat = self.queue.get()
            if resultat:
                self.resultatSignal.emit(resultat)
                self.enCours = False

    def kill(self) -> None:
        """
        stop le calcul et libère les ressources
        """
        if self.enCours:
            self.enCours = False
            self.process.kill()
            # print(self.process.pid)
            time.sleep(0.1)
            self.process.close()

class LevenshteinWidget(QWidget):
    """
    widget d'affichage du résultat du calcul de la distance de levenshtein
    """
    def __init__(self) -> None:
        super().__init__()
        self.nomSeq1 = ""
        self.nomSeq2 = ""
        self.dataSeq1 = ""
        self.dataSeq2 = ""
        self.resultat = -1

        self.init_ui()

    def init_ui(self) -> None:
        """
        initialisation des éléments graphiques
        """
        self.titre = QLabel(f"Distance de Levenshtein entre la séquence {self.nomSeq1} et {self.nomSeq2}")
        self.distanceLabel = QLabel("La distance de Levenshtein est en cours de calcul")

        vlayout = QVBoxLayout()
        vlayout.addWidget(self.titre)
        vlayout.addWidget(self.distanceLabel)
        vlayout.addStretch()
        self.setLayout(vlayout)

    def set_sequences(self, nomSeq1 : str, dataSeq1 : str,  nomSeq2 : str, dataSeq2 : str) -> None:
        """
        modificateur des données des séquences
        :param nomSeq1: nouveau nom de la séquence 1
        :type nomSeq1: str
        :param dataSeq1: nouvelle séquence 1
        :type dataSeq1: str
        :param nomSeq2: nouveau nom de la séquence 2
        :type nomSeq2: str
        :param dataSeq2: nouvelle séquence 2
        :type dataSeq2: str
        """
        self.nomSeq1 = nomSeq1
        self.dataSeq1 = dataSeq1
        self.nomSeq2 = nomSeq2
        self.dataSeq2 = dataSeq2
        self.titre.setText(f"Distance de Levenshtein entre la séquence {self.nomSeq1} et {self.nomSeq2}")

    def set_resultat(self, resultat : int) -> None:
        """
        modificateur du résultat de la distance de Levenshtein
        :param resultat: nouvelle valeur de la distance
        :type resultat: int
        """
        self.resultat = resultat
        self.distanceLabel.setText(f"La distance de Levenshtein est de {self.resultat}")
