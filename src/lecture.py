"""
Fichier principal
"""
from typing import List, Tuple
from Bio import SeqIO # type: ignore

def lire_fasta(nomFichier : str, numeroSequence : int = 0) -> Tuple[str,str]:
    """
    Lis un fichier Fasta et retourne le contenu de la séquence numeroSequence sous forme de tuple de string (id et séquence)

    :param nomFichier: le nom du fichier fasta à lire
    :type nomFichier: string
    :param numeroSequence: le numéro de la séquence à lire dans le fichier
    :type numeroSequence: int, 0 par défaut
    :raises ValueError: le numéro de la séquence à lire est incorrect
    :return: un tuple contenant l'id et la séquence
    :rtype: Tuple[str,str]
    """
    compteur : int = 0
    for sequence in SeqIO.parse(nomFichier, "fasta"):
        if compteur == numeroSequence:
            return (sequence.id,sequence.seq)
        compteur += 1
    raise ValueError('Le fichier ' + nomFichier + ' ne contient pas de séquence n°' + str(numeroSequence))

def lire_fasta_entier(nomFichier : str) -> List[Tuple[str,str]]:
    """
    Lis un fichier Fasta et retourne le contenu sous forme de tableau de tuple

    :param nomFichier: le nom du fichier fasta à lire
    :type nomFichier: string
    :return: un tableau de tuple contenant les id et les séquences
    :rtype: List[Tuple[str,str]]
    """
    sortie : List[Tuple[str,str]] = []
    for sequence in SeqIO.parse(nomFichier, "fasta"):
        sortie.append((sequence.id,sequence.seq))
    return sortie

def lire_texte(nomFichier : str) -> Tuple[str,str]:
    """
    Lis un fichier texte et retourne le contenu sous forme de tableau de tuple

    :param nomFichier: le nom du fichier fasta à lire
    :type nomFichier: string
    :return: un tuple contenant l'id et la séquence
    :rtype: Tuple[str,str]
    """
    fichier = open(nomFichier)
    sequence = "".join(fichier.readlines()).replace("\n","")
    fichier.close()
    nomSequence = ".".join(nomFichier.split("/")[-1].split(".")[:-1])
    return (nomSequence, sequence)
