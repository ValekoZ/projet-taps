""" classe du widget visualisation """
# pylint: disable=no-name-in-module,
from PyQt5.QtWidgets import QWidget, QTextEdit, QLabel, QVBoxLayout #type: ignore
from PyQt5.QtGui import QTextOption #type: ignore
from PyQt5.QtCore import Qt #type: ignore

from src.transcription import codons_en_acides_amines, T_PROTEINE

class Visualisation(QWidget):
    """
    visualisation de sequence
    """

    def __init__(self) -> None:
        super().__init__()

        self.init_ui()

    def init_ui(self) -> None:
        """
        initialisation des éléments graphiques
        """
        self.alignement = QTextEdit(readOnly=True)
        self.nom = QLabel()
        self.nom.setAlignment(Qt.AlignLeft)
        self.taille = QLabel()
        self.taille.setAlignment(Qt.AlignLeft)
        layout = QVBoxLayout()
        layout.addWidget(self.nom)
        layout.addWidget(self.taille)
        layout.addWidget(self.alignement)
        self.setLayout(layout)
        self.alignement.setWordWrapMode(QTextOption.WrapAnywhere)
        self.alignement.setFontFamily("monospace")
        self.alignement.setFontPointSize(20)
        self.alignement.cursorPositionChanged.connect(self.aide_acides)

    def aide_acides(self) -> None:
        """
        affiche les noms des acides de la séquence selectionnée
        """
        if self.nativeParentWidget().typeSelectionne == T_PROTEINE:
            compteur = 0
            copy = ""
            for i in codons_en_acides_amines(self.alignement.textCursor().selectedText(), False):
                copy += f"{i} "
                compteur += 1
                if compteur == 10:
                    copy += '\n'
            self.alignement.setToolTip(copy)

    def set_sequence(self, nom : str, seq : str) -> None:
        """
        modificateur de la sequence a afficher
        :param nom: nom de la séquence
        :type nom: str
        :param seq: donnée de la séquence
        :type seq: str
        """
        self.nom.setText(f"Nom : {nom}")
        self.taille.setText(f"Longueur : {len(seq)}")
        self.alignement.setText(seq)

    def reinitialiser(self) -> None:
        """
        reinitialise le widget sans séquence
        """
        self.nom.clear()
        self.taille.clear()
        self.alignement.clear()
