""" fichier contenant les classes necessaires à l'affichage de suites uniques et suites répétées """
# pylint: disable=no-name-in-module
from typing import Dict, List, Tuple
from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QVBoxLayout, QLineEdit, QFormLayout #type: ignore
from PyQt5.QtGui import QIntValidator #type: ignore
from PyQt5.QtCore import Qt, QThread, pyqtSignal #type: ignore

from src.stats import sequences_uniques, sequences_repetees


def max_repetitions(sequences: Dict[str, int]) -> Tuple[str, int]:
    """
    Retourne la séquence avec le plus de répétitions
    :param sequences: dictionnaire des sequences et de leur nombre d'apparition
    :type sequences: Dict[str, int]
    :rtype Tuple[str, int]
    """
    maxrep = ("", 0)
    for cle, valeur in sequences.items():
        if valeur > maxrep[1]:
            maxrep = (cle, valeur)
    return maxrep

class CalculSuitesUniqueRepetees(QThread):
    """
    Exectute les fonctions suites uniques et suites répétées dans un thread
    """
    resultatSignal = pyqtSignal(list,dict,tuple)

    def __init__(self, sequence1: str, longueur: int) -> None:
        super().__init__()
        self.sequence1 = sequence1
        self.longueur = longueur

    def run(self) -> None:
        """
        Retourne le resultat des fonctions suites uniques et suites repetees sur la sequence self.sequence1
        """
        suitesUniques = sequences_uniques(self.sequence1, self.longueur)
        suitesRepetees = sequences_repetees(self.sequence1, self.longueur)
        self.resultatSignal.emit(suitesUniques, suitesRepetees, max_repetitions(suitesRepetees))

class SuitesUniqueRepeteesWidget(QWidget):
    """
    Widget pour l'affichage des resultats de la fonction suite_unique
    """
    def __init__(self) -> None:
        super().__init__()
        self.nomSeq1 = ""
        self.dataSeq1 = ""
        self.longueur = 1
        self.calcul: CalculSuitesUniqueRepetees

        self.init_ui()

    def init_ui(self) -> None:
        """
        Définition de l'interface du plugin
        """
        self.titre = QLabel(f"Sous-séquences Uniques et sous-séquences Répétées dans la séquence {self.nomSeq1} (longueur : {len(self.dataSeq1)})")
        self.mainLabel = QLabel("Sélectionnez la taille des sous-séquences à analyser")
        vlayout = QVBoxLayout()
        vlayout.addWidget(self.titre)
        vlayout.addWidget(self.mainLabel)

        self.bouton = QPushButton("Calculer")
        self.bouton.clicked.connect(self.calculer)

        self.inputNombre = QLineEdit()
        self.inputNombre.setValidator(QIntValidator(bottom=1))
        self.inputNombre.setText("1")
        self.inputNombre.setAlignment(Qt.AlignLeft)
        self.inputNombre.textChanged.connect(self.texte_change_input)
        form = QFormLayout()
        form.addRow("Longueur des sous-séquences",self.inputNombre)
        form.addRow(self.bouton)

        self.statsSuitesUniquesLabel = QLabel("Nombre de séquences uniques:")
        form.addRow(self.statsSuitesUniquesLabel)
        vlayout.addWidget(self.statsSuitesUniquesLabel)

        self.statsSuitesRepetesLabel = QLabel("Nombre de séquences répétées:")
        form.addRow(self.statsSuitesRepetesLabel)
        vlayout.addWidget(self.statsSuitesRepetesLabel)

        self.sequenceLaPlusRepeteeLabel = QLabel("Séquence la plus répétée:")
        form.addRow(self.sequenceLaPlusRepeteeLabel)
        vlayout.addWidget(self.sequenceLaPlusRepeteeLabel)

        formWidget = QWidget()
        formWidget.setLayout(form)
        vlayout.addWidget(formWidget)
        vlayout.addStretch()

        self.setLayout(vlayout)

    def calculer(self) -> None:
        """
        Lance le calcul de la séquence
        """
        self.mainLabel.setText("Calculs en cours")
        self.bouton.setEnabled(False)
        self.calcul = CalculSuitesUniqueRepetees(self.dataSeq1, self.longueur)
        self.calcul.start()
        self.calcul.resultatSignal.connect(self.set_resultat)


    def set_sequences(self, nomSeq1 : str, dataSeq1 : str) -> None:
        """
        Changer la séquence
        :param nomSeq1: nom de la séquence
        :type nomSeq1: str
        :param dataSeq1: séquence
        :type dataSeq1: str
        """
        self.bouton.setEnabled(True)
        self.nomSeq1 = nomSeq1
        self.dataSeq1 = dataSeq1
        self.titre.setText(f"Répétitions de sous-séquences dans la séquence {self.nomSeq1} (longueur : {len(self.dataSeq1)})")
        self.inputNombre.setValidator(QIntValidator(bottom=1, top=len(self.dataSeq1)))

    def texte_change_input(self, texte: str) -> None:
        """
        Vérifie la valeur de la taille de séquenbce demandée par l'utilisateur
        :param texte: nombre ecrit par l'utilisateur
        :type texte: str
        """
        if texte != "":
            self.set_longueur(int(texte))

    def set_longueur(self, longueur: int) -> None:
        """
        Changer la taille des sous-séquences à dénombrer
        :param longueur: nouvelle longeur
        :type longueur: int
        """
        self.longueur = longueur

    def set_resultat(self, suitesUniques: List[str], suitesRepetees: Dict[str,int], maxRepetitions: Tuple[str, int]) -> None:
        """
        Afficher les résultats des calculs
        :param suitesUniques: liste des suites uniques
        :type suitesUniques: List[str]
        :param suitesRepetees: liste des dictionnaires, selon la taille, contenant les sequences et leur nombre d'apparition
        :type suitesRepetees: Dict[str,int]
        :param maxRepetitions: couple sequence et nombre de repetition de la sequence la plus répétée
        :type maxRepetitions: Tuple[str, int]
        """
        self.bouton.setEnabled(True)
        self.statsSuitesUniquesLabel.setText(f"Nombre de séquences uniques: {len(suitesUniques)}")
        self.statsSuitesRepetesLabel.setText(f"Nombre de séquences répétées: {len(suitesRepetees)}")
        self.sequenceLaPlusRepeteeLabel.setText(f"Séquence la plus répétée: {maxRepetitions[0]}\n{maxRepetitions[1]} répétitions")

        self.mainLabel.setText("")
