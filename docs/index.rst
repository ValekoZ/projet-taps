.. Projet TAPS documentation master file, created by
   sphinx-quickstart on Fri Nov  6 09:51:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Projet TAPS's documentation!
=======================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    README
    src
    tests
    CONTRIBUTING
    AUTHORS

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
