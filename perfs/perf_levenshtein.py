"""
Test des performances des fonctions de statistique
"""

from random import randint
from src.levenshtein import distance_efficace, distance_pour_test
from perfs.performances import Arg, generer_rapport

def rand_seq(longueur: int) -> str:
    """
    Génère une séquence de nucléotides aléatoire de taille comprise entre 20 et 100 bases

    :param longueur: la longueur de la séquence à retourner
    :type longueur: int
    :return: séquence de nucléotides (ATCG)
    :rtype: str
    """
    bases = "ATCG"
    return "".join([bases[randint(0, 3)] for _ in range(longueur)])


def perf_levenshtein_efficace():
    """
    Performances de la fonction de calcul de la distance de Levenshtein efficace
    """
    arg1 = Arg("sequences")
    arg1.choix_intervalle(10, 100000)
    arg1.generateurPasSuivant = lambda x: x * 10
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 10000
    arg1.xlog = True
    arg1.ylog = True

    listeArgs = [arg1, arg1]
    generer_rapport(distance_efficace, listeArgs, 5, "rapport")


def perf_levenshtein():
    """
    Performances de la fonction de calcul de la distance de Levenshtein utilisée pour les tests
    """
    arg1 = Arg("sequences")
    arg1.choix_intervalle(10, 100000)
    arg1.generateurPasSuivant = lambda x: x * 10
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 10000
    arg1.xlog = True
    arg1.ylog = True

    listeArgs = [arg1, arg1]
    generer_rapport(distance_pour_test, listeArgs, 5, "rapport")

perf_levenshtein_efficace()
perf_levenshtein()
