"""
Test des performances des fonctions de statistique
"""
from random import randint
from src.stats import denombrement, plus_longue_suite, sequences_repetees, sequences_uniques
from perfs.performances import Arg, generer_rapport


def rand_seq(longueur: int) -> str:
    """
    Génère une séquence de nucléotides aléatoire de taille comprise entre 20 et 100 bases

    :param longueur: la longueur de la séquence à retourner
    :type longueur: int
    :return: séquence de nucléotides (ATCG)
    :rtype: str
    """
    bases = "ATCG"
    return "".join([bases[randint(0, 3)] for _ in range(longueur)])

def perf_denombrement():
    """
    Performances de la fonction dénombrement avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10, 1000000)
    arg1.generateurPasSuivant = lambda x: x*10
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 100000
    arg1.xlog = True
    arg1.ylog = True

    arg2 = Arg("longueurSuiteMax")
    arg2.choix_intervalle(1, 250)
    arg2.generateurPasSuivant = lambda x: x+20
    arg2.generateurArgument = lambda x: x
    arg2.tailleDefaut = 10

    listeArgs = [arg1, arg2]
    generer_rapport(denombrement, listeArgs, 7, "rapport")

def perf_plus_longue_suite():
    """
    Performances de la fonction de recherche de la plus longue suite de fragment répété avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10000, 10000000)
    arg1.generateurPasSuivant = lambda x: x * 2
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 1000000
    arg1.xlog = True
    arg1.ylog = True

    arg2 = Arg("fragment")
    arg2.choix_intervalle(1, 1000000)
    arg2.generateurPasSuivant = lambda x: x * 2
    arg2.generateurArgument = rand_seq
    arg2.tailleDefaut = 10000
    arg2.xlog = True
    arg2.ylog = True

    listeArgs = [arg1, arg2]
    generer_rapport(plus_longue_suite, listeArgs, 10, "rapport")

def perf_sequences_repetees():
    """
    Performances de la fonction de recherche des séquences répétées avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10, 30000)
    arg1.generateurPasSuivant = lambda x: x * 2
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 1000
    arg1.xlog = True
    arg1.ylog = True

    arg2 = Arg("tailleSousSequence")
    arg2.generateurArgument = lambda x: x
    arg2.tailleDefaut = 10
    arg2.choix_intervalle(20, 221)
    arg2.generateurPasSuivant = lambda x: x+20
    arg2.generateurArgument = lambda x: x

    listeArgs = [arg1, arg2]
    generer_rapport(sequences_repetees, listeArgs, 10, "rapport")

def perf_sequences_uniques():
    """
    Performances de la fonction de recherche des séquences répétées avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10, 30000)
    arg1.generateurPasSuivant = lambda x: x * 2
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 1000
    arg1.xlog = True
    arg1.ylog = True

    arg2 = Arg("tailleSousSequence")
    arg2.generateurArgument = lambda x: x
    arg2.tailleDefaut = 10
    arg2.choix_intervalle(20, 221)
    arg2.generateurPasSuivant = lambda x: x+20
    arg2.generateurArgument = lambda x: x

    listeArgs = [arg1, arg2]
    generer_rapport(sequences_uniques, listeArgs, 10, "rapport")


perf_denombrement()
perf_plus_longue_suite()
perf_sequences_repetees()
perf_sequences_uniques()
