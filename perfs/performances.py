"""
Fonctions de test de performance
"""
import timeit
from typing import Dict, List, Callable, Any
import matplotlib.pyplot as plt # type: ignore

class Arg:
    """
    Generateur d'arguments pour la fonction performances
    Cette classe permet de générer des arguments de différents tailles/valeurs pour tester les performances d'une fonction
    """
    def __init__(self, nom):
        self.nom = nom                      # Le nom à afficher pour l'argument
        self.generateurArgument : Callable[int,Any]= lambda x:x         # La fonction qui pour un entier x génère un argument de taille x
        self.debut: int = 0                 # La taille minimale d'arguments à générer
        self.fin: int = 10                  # La taille maximale d'arguments à générer
        self.generateurPasSuivant: Callable[int,int] = lambda x: x+1    # La fonction qui à une taille x associe la taille suivante à tester
        self.tailleActuelle = self.debut    # La taille d'arguments actuelle
        self.tailleDefaut = 10              # La taille d'arguments à générer lorsque l'on teste les autres varibles
        self.xlog = False                   # Afficher les valeurs de les valeurs de l'axe x sur une échelle logarithmique
        self.ylog = False                   # Afficher les valeurs de les valeurs de l'axe y sur une échelle logarithmique
        self.skip = False                   # Ne pas faire de tests de performances sur cette variable
        self.estEnumerateur = False         # Si le générateur est un générateur d'arguments

    def pas_suivant(self) -> int:
        """
        Retourne le pas suivant de l'argument

        :return: la taille du prochain argument à générer
        :rtype: int
        """
        self.tailleActuelle = self.generateurPasSuivant(self.tailleActuelle)
        return self.tailleActuelle

    def argument(self, taille: int = -1) -> Any:
        """
        Retourne un argument de la taille demandé ou du pas actuel si rien n'est précisé

        :param taille: la taille de l'argument qu'on veut générer (défaut: self.tailleActuelle)
        :type taille: int
        :return: l'argument de la taille demandé
        :rtype: Any
        """
        if taille == -1:
            return self.generateurArgument(self.tailleActuelle)
        return self.generateurArgument(taille)

    def choix_intervalle(self, debut: int, fin: int):
        """
        Configure l'intervalle de valeurs pris par la taille de l'argument

        :param debut: la taille minimale de l'argument
        :type debut: int
        :param fin: la taille maximale de l'argument
        :type fin: int
        """
        self.debut = debut
        self.tailleActuelle = debut
        self.fin = fin

    def enumerateur(self, tableau : List[Any]):
        """
        Configure la fonction pour que les différents arguments en sortie soient les valeurs du tableau
        Le premier élément du tableau est la valeur par défaut

        :param tableau: la liste des arguments
        :type tableau: List[Any]
        """
        self.generateurArgument = lambda i: tableau[i]
        self.generateurPasSuivant = lambda x: x+1
        self.debut = 0
        self.tailleActuelle= 0
        self.tailleDefaut = 0
        self.fin = len(tableau)-1
        self.estEnumerateur = True

    def statique(self, valeur: Any):
        """
        Configure l'objet pour ne retourner qu'une seule valeur

        :param valeur: la valeur
        :type valeur: Any
        """
        self.enumerateur([valeur])

def mesure_temps(fonction: Callable, arguments: List[Any], repetitions: int = 10000) -> float:
    """
    Teste les performances d'une fonction en terme de temps

    :param fonction: la fonction dont on veut tester les performances
    :type fonction: callable
    :param arguments: une liste d'arguments à passer à la fonction
    :type arguments: List[any]
    :param repetitions: le nombre d'executions de la fonction
    :type repetitions: int
    :return: le temps d'exécution de la fonction en secondes
    :rtype: int
    """
    fonctionAvecArgs = lambda: fonction(*arguments)
    return timeit.timeit(fonctionAvecArgs, number=repetitions)/repetitions

def performances_fonction(fonction: Callable, arguments: List[Arg], repetitions: int = 1000) -> Dict[str,List[Dict[str,Any]]]:
    """
    Mesure les performances d'une fonction en faisant varier ses paramètres

    :param fonction: la fonction dont on veut tester les performances
    :type fonction: callable
    :param arguments: La liste des générateurs des arguments de la fonction
    :type fonction: List[Arg]
    :param repetitions: le nombre d'exécutions de la fonction pour améliorer la précision de la fonction
    :type repetitions: int
    """
    print(f"Tests de performance de la fonction {fonction.__name__}:")
    resultats : Dict[str,List[Dict[str,Any]]] = {x.nom:[] for x in arguments}
    argumentsParDefault = [x.argument(x.tailleDefaut) for x in arguments]
    for numArg,argument in enumerate(arguments):
        if argument.skip:
            continue
        args = argumentsParDefault[:]
        while argument.tailleActuelle <= argument.fin:
            args[numArg] = argument.argument()
            temps = mesure_temps(fonction, args, repetitions)
            print(f"Argument {argument.nom}, taille {argument.tailleActuelle} : {temps} s")
            resultats[argument.nom].append({'tailleArg': argument.tailleActuelle, "temps": temps})
            argument.pas_suivant()
    return resultats

def generer_graphique(axeX: List[int], axeY: List[int], nomVariable: str, nomFichier: str, xLog: bool = False, yLog : bool = False):
    """
    Génère un graphique des performances temporelles en fonction de la taille

    :param axeX: liste des valeurs des tailles d'arguments
    :type axeX: List[int]
    :param axeY: liste des valeurs des temps correspondants aux différentes tailles
    :type axeY: List[int]
    :param nomVariable: nom de la variable dont on a testé les performance
    :type nomVariable: str
    :param nomFichier: nom et chemin du fichier de sortie
    :type nomFichier: str
    :param xLog: afficher l'axe X avec une échelle logarithmique (par défaut: False)
    :type xLog: bool
    :param yLog: afficher l'axe Y avec une échelle logarithmique (par défaut: False)
    :type yLog: bool
    """
    plt.clf()
    plt.plot(axeX, axeY, '-', axeX, axeY ,'ro')
    plt.ylabel('Temps (s)')
    plt.xlabel('Taille')
    if xLog:
        plt.xscale("log")
    if yLog:
        plt.yscale("log")
    plt.title(f"Variable: {nomVariable}")
    plt.savefig(nomFichier)

def generer_rapport(fonction : Callable, arguments: List[Arg], repetitions: int, dossierSortie: str):
    """
    Génère un rapport sur les performances d'une fonction

    :param fonction: la fonction à tester
    :type nomFonction: Callable
    :param arguments: la liste des arguments de la fonction testée
    :type arguments: List[Arg]
    :param repetitions: le nombre d'exécutions de la fonction pour améliorer la précision de la fonction
    :type repetitions: int
    :param dossierSortie: le dossier dans lequel on génère les livrable
    :type dossierSortie: str
    """

    parametres = fonction.__annotations__
    listeParametres = []
    for k in parametres:
        varType = str(parametres[k])
        if k == "return":
            continue
        if '<class' in varType:
            varType = varType.split('\'')[1]
        elif 'typing.' in varType:
            varType = varType[7:]
        listeParametres.append({'nom': k, 'type': varType, 'default' : None, 'test': False, 'valeurs' : "/", 'defaut': '/'})
    for i,arg in enumerate(arguments):
        listeParametres[i]['test'] = "non" if arg.skip else "oui"
        if not arg.skip:
            if arg.estEnumerateur:
                listeParametres[i]['valeurs'] = ','.join([arg.argument(i) for i in range(arg.fin)])
                listeParametres[i]['defaut'] = arg.argument(0)
            else:
                listeParametres[i]['valeurs'] = f"Tailles: {arg.debut} à {arg.fin}"
                listeParametres[i]['defaut'] = arg.tailleDefaut
    performances = performances_fonction(fonction, arguments, repetitions)
    latex = f"""
    \\subsection{{Fonction \\textit{{{fonction.__name__}}}}}

    \\begin{{figure}}[H]
        \\centering
        \\begin{{table}}[H]
            \\centering
            \\begin{{tabular}}{{|l|l|l|l|l|}}
            \\hline
            \\textbf{{Argument}} & \\textbf{{Type}} & \\textbf{{Testé}} & \\textbf{{Valeurs}} & \\textbf{{Valeur par défaut}} \\\\
            \\hline
    """
    for param in listeParametres:
        latex += f"{param['nom']} & {param['type']} & {param['test']} & {param['valeurs']} & {param['defaut']}\\\\\n"
    latex+=f"""\\hline
            \\end{{tabular}}
        \\end{{table}}
        \\caption{{Listes des arguments de la fonction}}
        \\label{{fig:perfs_{ fonction.__name__ }_arguments}}
    \\end{{figure}}
    """
    for arg in arguments:
        if arg.skip:
            continue
        var = performances[arg.nom]
        axeX = []
        axeY = []
        for taille in var:
            axeX.append(taille['tailleArg'])
            axeY.append(taille['temps'])
        nomImage = f"{fonction.__name__}-{arg.nom}"
        generer_graphique(axeX,axeY, arg.nom, f"{dossierSortie}/performances/{nomImage}.svg", arg.xlog, arg.ylog)
        latex+=f"""
        \\begin{{figure}}[H]
            \\centering
            \\includesvg[width=1\\columnwidth]{{{dossierSortie}/performances/{nomImage}.svg}}
            \\caption{{Performances sur la variable {arg.nom}}}
            \\label{{fig:perfs_{ fonction.__name__ }_{arg.nom}}}
        \\end{{figure}}
        """
    fichierLatex = open(f"{dossierSortie}/performances/performances_{fonction.__name__}.tex", "w")
    fichierLatex.write(latex)
    fichierLatex.close()
