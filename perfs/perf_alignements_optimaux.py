"""
Test des performances des fonctions de statistique
Ces tests ne sont pas fait avec le module `perfs.performances` pour plus de flexibilité (plusieurs courbes par graphiques par exemple)
"""

from sys import setrecursionlimit
from timeit import timeit
import matplotlib.pyplot as plt # type: ignore
from src.needleman_wunsch import alignements_optimaux_rec_multiprocess, alignements_optimaux_rec_sauvegardes, alignements_optimaux_rec_simple, table_des_scores, alignements_optimaux

setrecursionlimit(10000)

def process(longueur):
    """
    Calcule les performances en fonction du nombre d'alignements
    """
    print(f"Longueur: {l}")
    sequenceA = "ACGT" + "A"*longueur + "CGT" + "A"*400
    sequenceB = "ACGTACGT" + "A"*400
    table = table_des_scores(sequenceA, sequenceB, [[1 if i==j else -3 for i in range(4)] for j in range(4)])

    print("alignements_optimaux_rec_multiprocess")
    temps['alignements_optimaux_rec_multiprocess'][longueur//5] = timeit(lambda: alignements_optimaux_rec_multiprocess(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER

    print("alignements_optimaux_rec_simple")
    temps['alignements_optimaux_rec_simple'][longueur//5] = timeit(lambda: alignements_optimaux_rec_simple(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER

    print("alignements_optimaux_rec_sauvegardes")
    temps['alignements_optimaux_rec_sauvegardes'][longueur//5] = timeit(lambda: alignements_optimaux_rec_sauvegardes(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER


def process_bis(longueur):
    """
    Calcule les performances en fonction de la longueur des séquences
    """
    print(f"Longueur: {l}")
    sequenceA = "A"*longueur
    sequenceB = "A"*longueur
    table = [[-3*j+4*i for i in range(j+1)] + [j-3*i for i in range(longueur-j)] for j in range(longueur+1)]

    assert len(list(alignements_optimaux(sequenceA, sequenceB, table=table))) == 1

    print("alignements_optimaux_rec_multiprocess")
    temps['alignements_optimaux_rec_multiprocess'][longueur//50-1] = timeit(lambda: alignements_optimaux_rec_multiprocess(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER

    print("alignements_optimaux_rec_simple")
    temps['alignements_optimaux_rec_simple'][longueur//50-1] = timeit(lambda: alignements_optimaux_rec_simple(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER

    print("alignements_optimaux_rec_sauvegardes")
    temps['alignements_optimaux_rec_sauvegardes'][longueur//50-1] = timeit(lambda: alignements_optimaux_rec_sauvegardes(sequenceA, sequenceB, table=table), number=NUMBER)/NUMBER

NUMBER = 10

longueurs = list(range(1, 100, 5))
temps = dict(alignements_optimaux_rec_multiprocess=longueurs.copy(), alignements_optimaux_rec_sauvegardes=longueurs.copy(), alignements_optimaux_rec_simple=longueurs.copy())


for l in longueurs:
    process(l)

plt.plot(longueurs, temps['alignements_optimaux_rec_simple'], label="Version simple")
plt.plot(longueurs, temps['alignements_optimaux_rec_sauvegardes'], label="Version avec sauvegardes")
plt.plot(longueurs, temps['alignements_optimaux_rec_multiprocess'], label="Version multiprocess")

plt.grid(True)

plt.legend()
plt.title("Evolution du temps d'execution en fonction\ndu nombre d'alignements optimaux")

plt.xlabel("Nombre d'alignements optimaux")
plt.ylabel("Temps d'execution")

plt.savefig("./rapport/performances/alignements_optimaux_selon_nombre_dalignements.svg")
plt.show()


longueurs = list(range(100, 1001, 100))
temps = dict(alignements_optimaux_rec_multiprocess=longueurs.copy(), alignements_optimaux_rec_sauvegardes=longueurs.copy(), alignements_optimaux_rec_simple=longueurs.copy())

NUMBER = 100

for l in longueurs:
    process_bis(l//2)

plt.plot(longueurs, temps['alignements_optimaux_rec_simple'], label="Version simple")
plt.plot(longueurs, temps['alignements_optimaux_rec_sauvegardes'], label="Version avec sauvegardes")
plt.plot(longueurs, temps['alignements_optimaux_rec_multiprocess'], label="Version multiprocess")

plt.grid(True)

plt.legend()
plt.title("Evolution du temps d'execution en fonction\nde la longueur des séquences")

plt.xlabel("Somme des longueurs des séquences")
plt.ylabel("Temps d'execution")

plt.savefig("./rapport/performances/alignements_optimaux_selon_longueurs.svg")
plt.show()

longueurs = list(range(100, 2001, 100))
listeTemps = [0.]*len(longueurs)
NUMBER = 500

GEN = None

for l in longueurs:
    print(f"Longueur: {l}")
    sequence = "A"*(l//2)
    tabl = [[0]*(l+1)]*(l+1)

    GEN = alignements_optimaux(sequence, sequence, table=tabl, similaritesEtTrou=([[-1]*5]*5, 0), loop=True)
    next(GEN)

    listeTemps[l//100-1] = timeit(lambda: GEN.send(True) if GEN else None, number=NUMBER)/NUMBER # pylint: disable=using-constant-test

plt.plot(longueurs, listeTemps)

plt.grid(True)

plt.title("Evolution du temps de passage d'un alignement à\nun autre avec le génerateur en fonction de la longueur des séquences")

plt.xlabel("Somme des longueurs des séquences (n+m)")
plt.ylabel("Temps d'execution")

plt.savefig("./rapport/performances/alignements_optimaux_selon_longueurs_generateur.svg")
plt.show()
