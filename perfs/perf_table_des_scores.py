# pylint: disable=cell-var-from-loop

"""
Test des performances des fonctions de statistique
Ces tests ne sont pas fait avec le module `perfs.performances` pour plus de flexibilité (plusieurs courbes par graphiques par exemple)
"""

import os
from timeit import timeit
import matplotlib.pyplot as plt # type: ignore
from src.needleman_wunsch import table_des_scores, table_des_scores_initial, table_des_scores_np_v1

r, w = os.pipe()

temps = dict()
longueurs = [50, 100, 200, 400, 800, 1600, 3200, 6400]

NUMBER = 10
DEFAULT = 500

MAX_FORK_INTERMEDIAIRE = 6

similarites = [[1 if i==j or max(i, j) == 4 else -3 for i in range(5)] for j in range(5)]


def save(ligne):
    """
    Sauvegarde une donnée dans `similarites` depuis une ligne du pipe
    """
    print(ligne)
    cle, tableau = ligne.split(':')
    temps[cle] = list(map(float, tableau.split(',')))


processId = os.fork()

if processId:
    print("Handling pipe")
    # Process parent

    os.close(w)

    with open(r, "r") as fd:
        for i in range(MAX_FORK_INTERMEDIAIRE + 4):
            save(fd.readline())

    plt.plot(longueurs, temps["table_des_scores_initial"], label="Première version")
    plt.plot(longueurs, list(map(lambda x: sum(x)/(MAX_FORK_INTERMEDIAIRE+1), zip(*[temps[f"table_des_scores_np_v1{i}"] for i in range(MAX_FORK_INTERMEDIAIRE)]))), label="Version intermediaire")
    plt.plot(longueurs, temps["table_des_scores"], label="Dernière version")

    plt.grid(True)

    plt.legend()
    plt.title("Comparaison entre les différentes\nversions de `table_des_scores`")

    plt.xlabel("Longueur de la première séquence")
    plt.ylabel("Temps d'execution")

    plt.xscale("log")
    plt.yscale("log")

    plt.savefig("./rapport/performances/comparaison_table_des_scores.svg")
    plt.show()

    plt.clf()

    plt.plot(longueurs, temps["table_des_scores"], label="Séquence A")
    plt.plot(longueurs, temps["table_des_scores_sym"], label="Séquence B")

    plt.grid(True)

    plt.legend()
    plt.title("Comparaison entre l'évolution du temps\npar rapport aux longueurs des 2 séquences")

    plt.xlabel("Longueur de la séquence")
    plt.ylabel("Temps d'execution")

    plt.xscale("log")
    plt.yscale("log")

    plt.savefig("./rapport/performances/comparaison_table_des_scores_sym.svg")
    plt.show()

else:
    # Process enfant

    os.close(r)

    processId = os.fork()

    if processId:
        print("Cas final")
        table = [timeit(lambda: table_des_scores("A"*l, "A"*DEFAULT, similarites), number=NUMBER)/NUMBER for l in longueurs]

        print("Cas final writing")
        with open(w, "w") as fd:
            fd.write("table_des_scores:" + ','.join(map(str, table)) + "\n")

    else:
        processId = os.fork()

        if processId:
            i = 0

            while i<MAX_FORK_INTERMEDIAIRE and not os.fork():
                i+=1

            print("Cas intermediaire")
            table = [timeit(lambda: table_des_scores_np_v1("A"*l, "A"*DEFAULT, similarites), number=NUMBER//(MAX_FORK_INTERMEDIAIRE+1))/(NUMBER//(MAX_FORK_INTERMEDIAIRE+1)) for l in longueurs]

            print("Cas intermediaire writing")
            with open(w, "w") as fd:
                fd.write(f"table_des_scores_np_v1{i}:" + ','.join(map(str, table)) + "\n")


        else:
            processId = os.fork()

            if processId:
                print("Cas de départ")
                table = [timeit(lambda: table_des_scores_initial("A"*l, "A"*DEFAULT, similarites), number=NUMBER)/NUMBER for l in longueurs]

                print("Cas de départ writing")
                with open(w, "w") as fd:
                    fd.write("table_des_scores_initial:" + ','.join(map(str, table)) + "\n")
            else:
                print("Cas sym final")
                table = [timeit(lambda: table_des_scores("A"*DEFAULT, "A"*l, similarites), number=NUMBER)/NUMBER for l in longueurs]

                print("Cas sym final writing")
                with open(w, "w") as fd:
                    fd.write("table_des_scores_sym:" + ','.join(map(str, table)) + "\n")
    os._exit(0) #pylint: disable=protected-access
