"""
Test des performances des fonctions de transcription
"""
from random import randint
from src.transcription import arn_en_adn, adn_en_arn, arn_en_codons
from perfs.performances import Arg, generer_rapport


def rand_seq(longueur: int, bases = "ATCG") -> str:
    """
    Génère une séquence de nucléotides aléatoire de taille comprise entre 20 et 100 bases

    :param longueur: la longueur de la séquence à retourner
    :type longueur: int
    :return: séquence de nucléotides (ATCG)
    :rtype: str
    """
    return "".join([bases[randint(0, len(bases)-1)] for _ in range(longueur)])

def perf_adn_en_arn():
    """
    Performances de la fonction adn_en_arn avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10, 100000000)
    arg1.generateurPasSuivant = lambda x: x*10
    arg1.generateurArgument = rand_seq
    arg1.tailleDefaut = 100000
    arg1.xlog = True
    arg1.ylog = True

    listeArgs = [arg1]
    generer_rapport(adn_en_arn, listeArgs, 10, "rapport")

def perf_arn_en_adn():
    """
    Performances de la fonction arn_en_adn avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(10, 100000000)
    arg1.generateurPasSuivant = lambda x: x*10
    arg1.generateurArgument = lambda x: rand_seq(x,"AUGC")
    arg1.tailleDefaut = 100000
    arg1.xlog = True
    arg1.ylog = True

    listeArgs = [arg1]
    generer_rapport(arn_en_adn, listeArgs, 10, "rapport")

def perf_arn_en_codons():
    """
    Performances de la fonction arn_en_codons avec plusieurs tailles d'arguments
    """
    arg1 = Arg("sequence")
    arg1.choix_intervalle(3, 300000000)
    arg1.generateurPasSuivant = lambda x: x*10
    arg1.generateurArgument = lambda x: rand_seq(x,"AUGC")
    arg1.tailleDefaut = 100000
    arg1.xlog = True
    arg1.ylog = True

    listeArgs = [arg1]
    generer_rapport(arn_en_codons, listeArgs, 10, "rapport")

perf_adn_en_arn()
perf_arn_en_adn()
perf_arn_en_codons()
