"""
Fichier de test pour les fonctions de l'algorithme de Needleman-Wunsch
"""

from random import choice, randint
import numpy as np

from src.needleman_wunsch import table_des_scores, alignements_optimaux_rec_simple, alignements_optimaux

SIMILARITES = [[1 if i == j else -2 if i == 4 or j == 4 else -3 for j in range(5)] for i in range(5)]


def test_table_des_scores_similaires():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des chaines identiques
    """

    chars = "ACGT"

    for _ in range(100):
        length = randint(1, 100)
        seq = "".join([choice(chars) for i in range(length)])
        table = table_des_scores(seq, seq, SIMILARITES)

        tablePredite = np.array([[-3 * i + 4 * j for j in range(i)] +
                        [i - 3 * j for j in range(length - i + 1)]
                        for i in range(length + 1)])

        assert (table == tablePredite).all()


def test_table_des_scores_trous_horiz():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des chaines similaires avec des trous horizontaux
    """

    for _ in range(100):
        lengthA = randint(50, 100)
        lengthB = randint(1, lengthA - 1)

        table = table_des_scores("A" * lengthA, "A" * lengthB, SIMILARITES)

        tablePredite = np.array([[-3 * i + 4 * j for j in range(i)] +
                        [i - 3 * j for j in range(lengthA - i + 1)]
                        for i in range(lengthB + 1)])

        assert (table == tablePredite).all()


def test_table_des_scores_trous_vert():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des chaines similaires avec des trous verticauxgit
    """

    for _ in range(100):
        lengthB = randint(5, 10)
        lengthA = randint(1, lengthB - 1)

        table = table_des_scores("A" * lengthA, "A" * lengthB, SIMILARITES)

        tablePredite = np.array([[-3 * i + 4 * j for j in range(min(i, lengthA + 1))] +
                        [i - 3 * j for j in range(lengthA - i + 1)]
                        for i in range(lengthB + 1)])

        assert (table == tablePredite).all()


def test_table_des_scores_diffs():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des chaines aucunement similaires
    """

    for _ in range(100):
        length = randint(1, 100)

        chaineA = "".join(choice("AC") for i in range(length))
        chaineB = "".join(choice("GT") for i in range(length))

        table = table_des_scores(chaineA, chaineB, SIMILARITES)

        tablePredite = np.array([[-3 * max(i, j) for i in range(length + 1)]
                        for j in range(length + 1)])

        assert (table == tablePredite).all()


def test_table_des_scores_inversion_mots():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des chaines aléatoires dans les 2 ordre : les 2 matrices obtenues doivent être transposée.
    """

    for _ in range(100):
        chaineA = "".join([choice("ACGT") for i in range(randint(1, 100))])
        chaineB = "".join([choice("ACGT") for i in range(randint(1, 100))])

        table1 = table_des_scores(chaineA, chaineB, SIMILARITES)
        table2 = table_des_scores(chaineB, chaineA, SIMILARITES)

        assert (table1 == [np.array(x) for x in zip(*table2)]).all()


def test_table_des_scores_inversion_limites():
    """
    Test de la fonction de calcul de la table des scores par l'algorithme de Needleman-Wunsch sur des cas limites
    """

    # Chaines vides:
    assert table_des_scores("", "", SIMILARITES) == [[0]]
    assert list(table_des_scores("", "ACGT", SIMILARITES)) == [[-3 * k] for k in range(5)]
    assert (table_des_scores("ACGT", "", SIMILARITES) == np.array([[-3 * k for k in range(5)]])).all()

def test_needleman_rec_simple():
    """
    Test de la fonction de calcul des séquences optimales par l'algorithme de Needleman-Wunsch récursif
    """

    # Cas limites
    motA = ""
    motB = "ATG" * 7
    assert alignements_optimaux_rec_simple(motA, motB) == [("-"*21, motB)]
    motB = ""
    assert alignements_optimaux_rec_simple(motA, motB)[0] == ("", "")

    # On vérifie que les alignements sont tous différents
    motA = "".join([choice("ACGT") for i in range(randint(1, 50))])
    motB = "".join([choice("ACGT") for i in range(randint(1, 50))])
    alignementPrecedent = []
    for alignemnt in alignements_optimaux_rec_simple(motA, motB):
        assert alignemnt != alignementPrecedent
        alignementPrecedent = alignemnt

    # On vérifie à l'aide d'un cas simple connu calcullé à la main
    motA = "ATCG"
    motB = "ATTTTCCCG"
    retour = [('AT---C--G', 'ATTTTCCCG'), ('A-T--C--G', 'ATTTTCCCG'), ('A--T-C--G', 'ATTTTCCCG'),
              ('A---TC--G', 'ATTTTCCCG'), ('AT----C-G', 'ATTTTCCCG'), ('A-T---C-G', 'ATTTTCCCG'),
              ('A--T--C-G', 'ATTTTCCCG'), ('A---T-C-G', 'ATTTTCCCG'), ('AT-----CG', 'ATTTTCCCG'),
              ('A-T----CG', 'ATTTTCCCG'), ('A--T---CG', 'ATTTTCCCG'), ('A---T--CG', 'ATTTTCCCG')]
    assert alignements_optimaux_rec_simple(motA, motB) == retour

def test_needleman():
    """
    Test de la fonction de calcul des séquences optimales par l'algorithme de Needleman-Wunsch
    """

    # Cas limites
    motA = ""
    motB = "ATG" * 7
    assert alignements_optimaux_rec_simple(motA, motB)[0] == ("-" * 21, motB)
    motB = ""
    assert alignements_optimaux_rec_simple(motA, motB)[0] == ("", "")

    # On vérifie que les alignements sont tous différents
    motA = "".join([choice("ACGT") for i in range(randint(1, 50))])
    motB = "".join([choice("ACGT") for i in range(randint(1, 50))])
    alignementPrecedent = []
    for alignemnt in alignements_optimaux(motA, motB):
        assert alignemnt != alignementPrecedent
        alignementPrecedent = alignemnt

    # On vérifie les résultats par cross-check
    for _ in range(5):
        motA = "".join([choice("ACGT") for i in range(randint(1, 50))])
        motB = "".join([choice("ACGT") for i in range(randint(1, 50))])
        assert list(alignements_optimaux(motA, motB)).sort() == alignements_optimaux_rec_simple(motA, motB).sort()
