"""
Tests sur le fichier de statistiques descriptives
"""
# pylint: disable=redefined-outer-name,invalid-name
import re
from random import randint
import pytest
from src.stats import denombrement_n, denombrement, sequences_repetees, sequences_uniques, plus_longue_suite


@pytest.fixture
def randSeq():
    """
    Génère une séquence de nucléotides aléatoire de taille comprise entre 20 et 100 bases
    """
    bases = "ATCG"
    return "".join([bases[randint(0, 3)] for _ in range(randint(20, 100))])

def test_denombrement_n(randSeq):
    """
    Tests des cas limites de la fonction denombrement_n
    """
    assert denombrement_n(randSeq, 0) == {'' : len(randSeq)}
    assert denombrement_n(randSeq, len(randSeq)) == {randSeq : 1}

def test_denombrement_erreurs():
    """
    On déclenche les erreurs de la fonction dénombrement
    """
    try:
        denombrement("une chaîne de caractères", -10)
    except ValueError as err:
        assert str(err) == "longueurSuiteMax incorrecte"

def test_denombrement(randSeq):
    """
    Vérifie les comptes de différentes sous-séquences sur une séquence aléatoire
    """
    # On appelle la fonction pour tout les nombres entre 1 et len(randSeq), puis pour 0 et pour len(randSeq)+1
    for i in range(len(randSeq) + 2):
        stats = denombrement(randSeq, i)

        # On vérifie le nombre d'éléments renvoyés
        assert len(stats) == i

        # Pour chaque élément, on vérifie que les résultats sont cohérents
        for j in range(1,i):
            randSeqDecoupee = [
                randSeq[k:k + j] for k in range(len(randSeq) - j + 1)
            ]
            for frag, compte in stats[j].items():
                assert compte == randSeqDecoupee.count(frag)# On vérifie les comptes

            for frag in randSeqDecoupee:
                assert frag in stats[
                    j]  # Et on vérifie que toutes les sous-chaînes sont bien prises en compte

def test_plus_longue_suite():
    """
    Test que la fonction plus_longue_suite retourne bien la plus longue occurence de randFrag dans randSeq
    """
    bases = "ATCG"
    randSeq = [bases[randint(0, 3)] for _ in range(randint(20, 100))]
    p = [
        [randSeq, randSeq, 0, 1],                               # sous-séquence = séquence
        ["", "", -1, 0],                                       # séquence = sous-séquence = vide
        ["", "ATCG", -1, 0],                                   # séquence vide
        ["ATCG", "", -1, 0],                                   # sous-séquence vide
        ["AAAA", "CCCCC", -1, 0],                              # sous-séquence de taille > à taille de séquence
        ["ATCG", "ATCGG", -1, 0],                              # sous-séquence = sequence + un caractère
        ["AAAAAA", "A", 0, 6],                                  # séqence = n * sous-séqence et sous-séquence = un caractère
        ["ATATATAT", "AT", 0, 4],                               # sous-séquence = 2 caractères et séquence = n * sous-séquence
        ["AATTATATA", "AT", 4, 2],                              # séqence = . + sous-séquence * n + .
        ["ATATATTTTA", "ATAT", 0, 1],                           # séquence = sous-séquence * 1.5
        ['ATATATTTTA', "AT", 0, 3],                             # séquence = sous-séquence * n + . * m
        ["CAATATATATTAATATATCCG", "AT", 2, 4],                  # présence de n et m sous-séqence dans séquence ou n > m
        ["AGTATATATTTTATATATAT", "AT", 12, 4],                  # présence de n et m sous-séquence dans séquence ou m > n
        ["CAATATATTAATATATCCG", "AT", 2, 3],                    # présence de n et m sous-séqence dans séquence ou n = m
        ["ATCGATATATCGATATATATATCGATATATATCG", "AT", 12, 5],    # présence de l, n, m sous-séquence ou l < m < n
    ]

    for l in p:
        indiceDeb, nbRepet = plus_longue_suite(l[0], l[1])
        assert indiceDeb == l[2]
        assert nbRepet == l[3]

    for _ in range(100):
        randSeq = "".join([bases[randint(0, 3)] for _ in range(randint(20, 100))])
        randFrag = "".join([bases[randint(0, 3)] for _ in range(randint(1, len(randSeq)//2))])

        indiceDeb, nbRepet = plus_longue_suite(randSeq, randFrag)
        # cas ou la sous-séquence est trouvée
        if nbRepet > 0:
            p1 = re.compile(randFrag * nbRepet)
            m1 = p1.search(randSeq)
            assert indiceDeb == m1.start()
        else:
            assert indiceDeb == -1
        # on recherche si l'on peut trouver la sous-séquence répétée au moins nbRepet+1 fois
        p2 = re.compile(randFrag * (nbRepet + 1))
        m2 = p2.search(randSeq)
        assert m2 is None

def test_sequences_repetees_limites():
    """
    On teste les cas limites de la fonction sequences_repetees
    """
    assert sequences_repetees("",0) == {}

def test_sequences_repetees(randSeq):
    """
    Test la sequences_repetees en comparant son résultat à une fonction qui à le même fonctionnement (crosscheck), et vérifie les cas limites
    """
    assert sequences_repetees("ATCG",1) == {}
    assert sequences_repetees(randSeq, len(randSeq) - 1) == {c:v for c,v in denombrement_n(randSeq, len(randSeq)-1).items() if v>1}

def test_sequences_uniques_limites():
    """
    On teste les cas limites de la fonction sequences_uniques
    """
    assert sequences_uniques("",0) == []

def test_sequences_uniques(randSeq):
    """
    Test la sequences_repetees en comparant son résultat à une fonction qui à le même fonctionnement (crosscheck), et vérifie les cas limites
    """
    assert sequences_uniques("ATCG",1) == ["A","T", "C", "G"]
    assert sequences_uniques(randSeq, len(randSeq) - 1) == [c for c,v in denombrement_n(randSeq, len(randSeq)-1).items() if v==1]
