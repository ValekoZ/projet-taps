"""
Tests sur les fonctions de transcription
"""
# pylint: disable=redefined-outer-name,invalid-name
from random import randint
import pytest
from src.transcription import adn_en_arn, arn_en_adn, arn_en_codons, codons_en_acides_amines


def test_adn_en_arn_cas_limites():
    """
    Test des cas limites de la fonction de traduction de l'ADN en ARN
    """
    assert adn_en_arn("") == ""

def test_adn_en_arn_crosscheck():
    """
    Test de la transcription de l'ADN en ARN (crosscheck)
    """
    bases = "ATCG"
    seq =  "".join([bases[randint(0,3)] for _ in range(randint(20,100))])
    #Pour éviter que tous les C et les G ne deviennent des C on passe par un caractère intermédiaire
    assert adn_en_arn(seq) == seq.replace("A","U").replace("T","A").replace("C","X").replace("G","C").replace("X","G")

def test_adn_en_arn_inverse():
    """
    On vérifie le fonctionnement de la fonction en utilisant une fonction qui redonne la chaîne de départ
    """
    bases = "ATCG"
    seq =  "".join([bases[randint(0,3)] for _ in range(randint(20,100))])
    trad = adn_en_arn(seq)
    assert seq == trad.replace("A","T").replace("U","A").replace("G","X").replace("C","G").replace("X","C")

def test_adn_en_arn_erreur():
    """
    Vérifie que la fonction adn_en_arn soulève une erreur lorsque la chaîne en entrée est incorrecte
    """
    mauvaisesChaines = ["ATGCTGACGATCGATGCUCGATCGA", "bonjour", "atgcgc"]
    for chaine in mauvaisesChaines:
        try:
            adn_en_arn(chaine)
        except ValueError as err:
            assert str(err) == "La séquence d'ADN contient des bases incorrectes"

@pytest.fixture
def randSeq():
    """
    Génère une séquence de nucléotides aléatoire de taille comprise entre 20 et 100 bases
    """
    bases = "ATCG"
    return "".join([bases[randint(0, 3)] for _ in range(randint(20, 100))])

def test_arn_en_adn(randSeq):
    """
    Test de la transcription de l'ADN en ARN
    """
    assert randSeq == arn_en_adn(adn_en_arn(randSeq))

def test_arn_en_codons_cas_limites():
    """
    Test des cas limites de la fonction de traduction de l'ARN en codons
    """
    assert arn_en_codons("") == [""]

def test_arn_en_codons():
    """
    Génère un couple séquence d'arn et séquence de codons correspondante pour vérifier la fonction arn_en_codons
    """
    couplesArnCodons = [
        ("UUU","F"), ("UUC","F"), ("UUA","L"), ("UUG","L"),
        ("UCU","S"), ("UCC","S"), ("UCA","S"), ("UCG","S"),
        ("UAU","Y"), ("UAC","Y"), ("UAA","*"), ("UAG","*"),
        ("UGU","C"), ("UGC","C"), ("UGA","*"), ("UGG","W"),
        ("CUU","L"), ("CUC","L"), ("CUA","L"), ("CUG","L"),
        ("CCU","P"), ("CCC","P"), ("CCA","P"), ("CCG","P"),
        ("CAU","H"), ("CAC","H"), ("CAA","Q"), ("CAG","Q"),
        ("CGU","R"), ("CGC","R"), ("CGA","R"), ("CGG","R"),
        ("AUU","I"), ("AUC","I"), ("AUA","I"), ("AUG","M"),
        ("ACU","T"), ("ACC","T"), ("ACA","T"), ("ACG","T"),
        ("AAU","N"), ("AAC","N"), ("AAA","K"), ("AAG","K"),
        ("AGU","S"), ("AGC","S"), ("AGA","R"), ("AGG","R"),
        ("GUU","V"), ("GUC","V"), ("GUA","V"), ("GUG","V"),
        ("GCU","A"), ("GCC","A"), ("GCA","A"), ("GCG","A"),
        ("GAU","D"), ("GAC","D"), ("GAA","E"), ("GAG","E"),
        ("GGU","G"), ("GGC","G"), ("GGA","G"), ("GGG","G")
    ]
    arn = ""
    codons = []
    suiteCodons = ""
    for _ in range(randint(20,100)):
        (bases, codon) = couplesArnCodons[randint(0, len(couplesArnCodons)-1)]
        if codon == "*":
            codons.append(suiteCodons)
            suiteCodons = ""
        else:
            suiteCodons += codon
        arn += bases
    codons.append(suiteCodons)
    assert arn_en_codons(arn) == codons

def test_arn_en_codons_erreur_longueur():
    """
    Vérifie que la fonction arn_en_codons retourne bien une erreur si la longueur des séquences est incorrecte
    """
    bases = "AUCG"
    sequences = ["".join([bases[randint(0,3)] for _ in range(randint(20,100))]) for _ in range(30)]
    for seq in sequences:
        if len(seq)%3 != 0:
            try:
                arn_en_codons(seq)
                assert False
            except ValueError as err:
                assert str(err) == "La longueur de la séquence d'ARN est incorrecte"

def test_arn_en_codons_erreur_bases():
    """
    Vérifie que la fonction arn_en_codons retourne bien une erreur si les séquences d'ARN contiennent des bases incorrectes
    """
    mauvaisesChaines = ["AUGUCUCGUEGC", "coucou", "atgcgc"]
    for chaine in mauvaisesChaines:
        try:
            arn_en_codons(chaine)
        except ValueError as err:
            assert str(err) == "La séquence d'ARN contient des bases incorrectes"

def test_condons_en_acides_amines_cas_limites():
    """
    Test des cas limites de la fonction de traduction des codons en acides aminés
    """
    assert codons_en_acides_amines("") == []

def test_codons_en_acides_amines():
    """
    Vérifie la fonction codons_en_acides_amines
    """
    associations = [
        ("F", "Phe", "Phénylalanine"),
        ("L", "Leu", "Leucine"),
        ("I", "Ile", "Isoleucine"),
        ("M", "Met", "Méthionine"),
        ("V", "Val", "Valine"),
        ("S", "Ser", "Sérine"),
        ("P", "Pro", "Proline"),
        ("T", "Thr", "Thréonine"),
        ("A", "Ala", "Alanine"),
        ("Y", "Tyr", "Tyrosine"),
        ("H", "His", "Histidine"),
        ("Q", "Gln", "Glutamine"),
        ("N", "Asn", "Asparagine"),
        ("K", "Lys", "Lysine"),
        ("D", "Asp", "Acide aspartique"),
        ("E", "Glu", "Acide glutamique"),
        ("C", "Cys", "Cystéine"),
        ("W", "Trp", "Tryptophane"),
        ("R", "Arg", "Arginine"),
        ("G", "Gly", "Glycine"),
        ("*", "STOP","STOP")
    ]
    codons = ""
    acidesAminesAbrev = []
    acidesAminesLong = []
    for _ in range(randint(20,100)):
        (codon, abreviation, formeLongue) = associations[randint(0, len(associations)-1)]
        codons += codon
        acidesAminesAbrev.append(abreviation)
        acidesAminesLong.append(formeLongue)
    assert codons_en_acides_amines(codons) == acidesAminesAbrev
    assert codons_en_acides_amines(codons, False) == acidesAminesLong

def test_codons_en_acides_amines_erreur_bases():
    """
    Vérifie que la fonction codons_en_acides_amines retourne bien une erreur si les séquences contiennent des codons incorrects
    """
    mauvaisesChaines = ["FLYPTHFFUJ", "coucou", "atgcgc"]
    for chaine in mauvaisesChaines:
        try:
            codons_en_acides_amines(chaine)
        except ValueError as err:
            assert str(err) == "La séquence contient des codons incorrects"
