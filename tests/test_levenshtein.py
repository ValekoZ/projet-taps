"""
Fichier test des fonctions de calcul de la distance de Levenshtein
"""

from random import randint
from src.levenshtein import distance_efficace, distance_pour_test

def levenshtein_rec(mot1, mot2) -> int:
    """
    Implémentation récursive de la distance de Levensthein
    Source : https://blog.finxter.com/how-to-calculate-the-levenshtein-distance-in-python/
    """
    if not mot1:
        return len(mot2)
    if not mot2:
        return len(mot1)
    return min(levenshtein_rec(mot1[1:], mot2[1:])+(mot1[0] != mot2[0]),
               levenshtein_rec(mot1[1:], mot2)+1,
               levenshtein_rec(mot1, mot2[1:])+1)

def test_distance_pour_test():
    """
    Test de la fonction de calcul de la distance de Levenshtein de manière exhaustive
    """
    motA = ""
    motB = "AZD" * 7
    assert distance_pour_test(motA, motB) == 3 * 7
    motB = ""
    assert distance_pour_test(motA, motB) == 0

    for taille in range(1, 15):
        motA = ""
        motB = ""
        for _ in range(taille):
            motA += chr(randint(65, 65 + 25))
        for lettre in range(randint(taille - 50, taille)):
            identique = randint(0, 2) # Une lettre sur deux en moyenne est la même
            if identique == 0:
                motB += chr(randint(65, 65 + 25))
            else:
                motB += motA[lettre]
        assert distance_pour_test(motA, motB) == levenshtein_rec(motA, motB)


def test_distance():
    """
    Test de la fonction efficace de calcul de la distance de Levenshtein
    """
    motA = ""
    motB = "AZD"*7
    assert distance_pour_test(motA, motB) == 7 * 3
    motB = ""
    assert distance_pour_test(motA, motB) == 0

    for taille in range(100, 250): # On peut tester sur de plus grandes chaines sans la focntion récursive
        motA = ""
        motB = ""
        for _ in range(taille):
            motA += chr(randint(65, 65 + 25))
        for _ in range(taille - 10, randint(taille - 9, taille + 50)):
            motB += chr(randint(65, 65 + 25))
        assert distance_pour_test(motA, motB) == distance_efficace(motA, motB)
