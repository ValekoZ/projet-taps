"""
Tests du fichier principal
"""
# pylint: disable=redefined-outer-name,invalid-name
from src.lecture import lire_fasta, lire_fasta_entier, lire_texte


def test_lire_fasta():
    """
    Test de lecture d'une séquence dans un fichier fasta
    """
    donnees = lire_fasta("donnees/sequenceMT534307.fasta", 0)
    assert donnees[0] == "MT534307.1"
    assert len(donnees[1]) == 29901


def test_lire_fasta_erreur():
    """
    On tente d'acceder à une séquence qui n'existe pas
    """
    try:
        lire_fasta("donnees/sequenceMT534307.fasta", 2)
    except ValueError as err:
        assert str(err) == "Le fichier donnees/sequenceMT534307.fasta ne contient pas de séquence n°2"


def test_lire_fasta_entier():
    """
    Test de lecture d'un fichier fasta entier
    """
    donnees = lire_fasta_entier("donnees/sequenceMT534307.fasta")
    assert len(donnees) == 1
    assert donnees[0][0] == "MT534307.1"
    assert len(donnees[0][1]) == 29901

def test_lire_txt():
    """
    Test de la lecture d'une séquence dans un fichier texte
    """
    donnees = lire_texte("donnees/NC_045512.2.txt")
    assert donnees[0] == "NC_045512.2"
    assert len(donnees[1]) == 29903
