Comment contribuer au projet
============================

Ajouter une fonctionnalité


Toutes les fonctionnalités doivent être programmées dans des branches séparées (**pas de commit directs sur master!**) qui seront ensuite fusionnées avec la branche maître si le reste de l'équipe valide la modification.

Nomenclature des commits
------------------------

Tous les noms de commits doivent possèder un préfix détaillant leur type. Les préfix sont détaillés dans le tableau ci-dessous.

+--------+----------------------------------------------------------------------------------------------+
| Préfix | Type de commit                                                                               |
+========+==============================================================================================+
| [ADD]  | Ajout d'une nouvelle fonctionnalité ou d'un nouveau fichier                                  |
+--------+----------------------------------------------------------------------------------------------+
| [DEL]  | Suppression d'une fonctionnalité ou d'un fichier                                             |
+--------+----------------------------------------------------------------------------------------------+
| [FIX]  | Correction d'un bug                                                                          |
+--------+----------------------------------------------------------------------------------------------+
| [IMP]  | Amélioration d'une fonctionnalité existante                                                  |
+--------+----------------------------------------------------------------------------------------------+
| [DOC]  | Modification en rapport avec de la documentation (commmentaires, docstring ou fichiers .md)  |
+--------+----------------------------------------------------------------------------------------------+
| [CLN]  | Nettoyage du code                                                                            |
+--------+----------------------------------------------------------------------------------------------+
| [TYPO] | Correction d'une faute de frappe ou d'orthographe sans incidence sur le code                 |
+--------+----------------------------------------------------------------------------------------------+
| [TEST] | Ajout de tests                                                                               |
+--------+----------------------------------------------------------------------------------------------+
| [ENV]  | Modifications en rapport avec l'environnement du projets (*.gitignore*...)                   |
+--------+----------------------------------------------------------------------------------------------+

Fusion de branche sur master
----------------------------

Pour fusionner une branche avec la branche *master*, le code à fusionner doit passer les tests unitaires qui lui sont associés. Il doit de plus être relu par au moins deux autres membres du groupe qui doivent donner leur approbation explicite dans la *pull request* correspondante (penser à ajouter minimum 2 validateurs).

Formatage du code et conventions
--------------------------------

Les commentaires et nom de fonctions sont rédigés en français.

Toutes les fonctions doivent être typés (*notations Python >=3.6*)

Les fonctions doivent suivre la notation *snake_case* tandis que les variables doivent suivre la notation *camelCase*.

Les docstrings repectent la convention Sphinx::

    """[Summary]
    
    :param [ParamName]: [ParamDescription], defaults to [DefaultParamVal]
    :type [ParamName]: [ParamType](, optional)
    ...
    :raises [ErrorType]: [ErrorDescription]
    ...
    :return: [ReturnDescription]
    :rtype: [ReturnType]
    """

