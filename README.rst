Projet TAPS
===========

.. image:: https://gitlab.telecomnancy.univ-lorraine.fr/taps2k21/project-grp14/badges/master/pipeline.svg

Documentation
-------------

Vous pouvez lire la documentation ici_.

.. _ici: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/index.html


Règles d'édition du dépot
-------------------------

Voir CONTRIBUTING.rst_

.. _CONTRIBUTING.rst: /CONTRIBUTING.rst


Tester son code en local
------------------------

Pour réaliser les vérifications faites par le pipeline en local:

.. code-block:: bash

    pytest && mypy src/ && pylint src/ tests/

Rapport
-------

`Télécharger le rapport au format pdf`_

.. _`Télécharger le rapport au format pdf`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/rapport.pdf

`Télécharger le WBS au format pdf`_

.. _`Télécharger le WBS au format pdf`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/wbs.pdf

`Télécharger le GANTT au format pdf`_

.. _`Télécharger le GANTT au format pdf`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/GANTT.pdf


Comptes rendus de réunions
--------------------------

`Compte rendu de réunion du 22/10/2020`_

.. _`Compte rendu de réunion du 22/10/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_10_22.pdf


`Compte rendu de réunion du 05/11/2020`_

.. _`Compte rendu de réunion du 05/11/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_11_05.pdf


`Compte rendu de réunion du 19/11/2020`_

.. _`Compte rendu de réunion du 19/11/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_11_19.pdf


`Compte rendu de réunion du 23/12/2020`_

.. _`Compte rendu de réunion du 23/12/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_12_23.pdf


`Compte rendu de réunion du 28/12/2020`_

.. _`Compte rendu de réunion du 28/12/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_12_28.pdf


`Compte rendu de réunion du 31/12/2020`_

.. _`Compte rendu de réunion du 31/12/2020`: http://taps2k21.gl-pages.telecomnancy.univ-lorraine.fr/project-grp14/rapports/reunion-2020_12_31.pdf


Dates
-----

- Date de dépôt des groupes : 23 octobre 2020, 18:00 CEST
- Date de rendu de projet : Mercredi 6 janvier 2021, 23:00 CEST
- Date de soutenance FISE : Lundi 11 janvier 2021
